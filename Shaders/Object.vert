#version 450

layout(location = 0) in vec3 attrPos;
layout(location = 1) in vec3 attrNorm;

layout(std140,binding=0) uniform Camera
{
    uniform mat4 projView;
    uniform vec3 cameraPos;
}; 

layout(location = 0) uniform mat4 uniModel;
layout(location = 2) uniform mat4 uniOrient;
layout(location = 0) out vec3 outNorm;



void main()
{
    gl_Position = projView * uniModel * vec4(attrPos,1);
    outNorm = (uniOrient * vec4(attrNorm,1)).xyz;
}