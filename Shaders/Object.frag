#version 450
layout(location = 0) out vec4 fragColor;

layout(location = 1) uniform vec4 uniColor;

layout(location = 0) in vec3 inNorm;

vec3 lightDirection = vec3(0.707,0.707,0);

void main()
{
    float light =(dot(lightDirection,inNorm)+1)/2;
    if(uniColor.a != 1) light = 1;
    fragColor = vec4(uniColor.rgb*light,uniColor.a);
}