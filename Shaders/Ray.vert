#version 450

layout(location = 0) in vec3 attrPos;
layout(location = 1) in vec3 attrColor;
layout(location = 0) out vec3 outColor;
layout(location = 0) uniform mat4 uniModel;

layout(std140,binding=0) uniform Camera
{
    uniform mat4 projView;
    uniform vec3 cameraPos;
}; 


void main()
{
    gl_Position = projView *  vec4(attrPos,1);
    outColor = attrColor;
}