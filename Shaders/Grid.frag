#version 450

layout(location = 0) out vec4 fragColor;

void main()
{
    float dist = gl_FragCoord.z / gl_FragCoord.w;
	float v = 1 - dist / 150000.0;
    fragColor = vec4(0.22,0.58,0.89,1)*v;
}