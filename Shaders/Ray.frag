#version 450

layout(location = 0) out vec4 fragColor;
layout(location = 0) in vec3 inColor;

void main()
{
    fragColor = vec4(inColor,0.5);
}