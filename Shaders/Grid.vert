#version 450

layout(location = 0) in vec3 attrPos;

layout(std140,binding=0) uniform Camera
{
    uniform mat4 projView;
    uniform vec3 cameraPos;
}; 


void main()
{
    gl_Position = projView * vec4(attrPos+vec3(floor(cameraPos.x/1000.0)*1000.0,0,floor(cameraPos.z/1000.0)*1000.0),1);
}