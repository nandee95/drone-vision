
function(color_message)
    set(ARG_LIST "")

    list(GET ARGV 0 MESSAGE_TYPE)
    if("${MESSAGE_TYPE}" MATCHES "^(FATAL_ERROR|SEND_ERROR)$")
        list(REMOVE_AT ARGV 0)
        list(APPEND ARG_LIST --red)
    else()
        set(MESSAGE_TYPE "")
    endif()

    string(REGEX REPLACE "%(normal|black|red|green|yellow|blue|magenta|cyan|white)" ";" MESSAGE_PARTS "${ARGV}")
    string(REGEX MATCHALL "%(normal|black|red|green|yellow|blue|magenta|cyan|white)" MESSAGE_COLORS "${ARGV}")

    if("${ARGV}" MATCHES "^%(normal|black|red|green|yellow|blue|magenta|cyan|white)")
        list(GET MESSAGE_COLORS 0 COLOR)
        string(SUBSTRING ${COLOR} 1 -1 COLOR)
        list(APPEND ARG_LIST --${COLOR})
        list(REMOVE_AT MESSAGE_COLORS 0)
    endif()

    foreach(MSG ${MESSAGE_PARTS})
        list(APPEND ARG_LIST "${MSG}")
        if(MESSAGE_COLORS)
            list(GET MESSAGE_COLORS 0 COLOR)
            string(SUBSTRING ${COLOR} 1 -1 COLOR)
            list(APPEND ARG_LIST --${COLOR})
            list(REMOVE_AT MESSAGE_COLORS 0)
        endif()
    endforeach()

    execute_process(COMMAND 
        ${CMAKE_COMMAND} -E env CLICOLOR_FORCE=1
        ${CMAKE_COMMAND} -E cmake_echo_color --no-newline ${ARG_LIST}
    )

    if(MESSAGE_TYPE)
        string(REGEX REPLACE "%(normal|black|red|green|yellow|blue|magenta|cyan|white)" "" MESSAGE_STRIPPED "${ARGV}")
        message(${MESSAGE_TYPE} ${MESSAGE_STRIPPED})
    endif()
endfunction()
