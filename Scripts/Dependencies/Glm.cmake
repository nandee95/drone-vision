set(GLM_EXT_TARGET "dep_glm")
set(GLM_TARGET "Glm")

if(TARGET ${GLM_TARGET})
	return()
endif()

color_message("Loading Glm... ")

# Options
option(GLM_EXTERNALPROJECT "Obtain Glm from github.com/g-truc/glm" ON)

# Create target
add_library(${GLM_TARGET} INTERFACE)

# Load external project
if(${GLM_EXTERNALPROJECT})
	set(GLM_INSTALL_DIR "${PROJECT_DEPENDENCY_DIR}/Glm")
	include(ExternalProject)
	ExternalProject_Add(
		${GLM_EXT_TARGET}
		PREFIX ${GLM_INSTALL_DIR}
		GIT_REPOSITORY "https://github.com/g-truc/glm.git"
		CONFIGURE_COMMAND ""
		BUILD_COMMAND ""
		INSTALL_COMMAND ""
	)
	set_target_properties(${GLM_EXT_TARGET} PROPERTIES FOLDER "Dependencies")
	ExternalProject_Get_Property(${GLM_EXT_TARGET} SOURCE_DIR)

	# Create target
	target_include_directories(${GLM_TARGET} INTERFACE ${SOURCE_DIR})
	if(NOT ${DEV_IGNORE_DEPENENDENCIES})
		add_dependencies(${GLM_TARGET} ${GLM_EXT_TARGET})
	endif()

	color_message("%greenOK (ExternalProject)\n")
	return()
endif()

color_message("%redNO METHOD\n")
color_message(FATAL_ERROR "No method defined to load Glm.\n")