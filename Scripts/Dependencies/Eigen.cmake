set(EIGEN_EXT_TARGET "dep_eigen")
set(EIGEN_TARGET "Eigen")

if(TARGET ${EIGEN_TARGET})
	return()
endif()

color_message("Loading Eigen... ")

# Options
option(EIGEN_EXTERNALPROJECT "Obtain Eigen from https://gitlab.com/libeigen/eigen.git" ON)

# Create target
add_library(${EIGEN_TARGET} INTERFACE)

# Load external project
if(${EIGEN_EXTERNALPROJECT})
	set(EIGEN_INSTALL_DIR "${PROJECT_DEPENDENCY_DIR}/Eigen")
	include(ExternalProject)
	ExternalProject_Add(
		${EIGEN_EXT_TARGET}
		PREFIX ${EIGEN_INSTALL_DIR}
		GIT_REPOSITORY "https://gitlab.com/libeigen/eigen.git"
		CMAKE_ARGS
			-DCMAKE_INSTALL_PREFIX=${EIGEN_INSTALL_DIR}
	)
	set_target_properties(${EIGEN_EXT_TARGET} PROPERTIES FOLDER "Dependencies")
	ExternalProject_Get_Property(${EIGEN_EXT_TARGET} SOURCE_DIR)

	# Create target
	target_include_directories(${EIGEN_TARGET} INTERFACE ${EIGEN_INSTALL_DIR}/include/eigen3)
	if(NOT ${DEV_IGNORE_DEPENENDENCIES})
		add_dependencies(${EIGEN_TARGET} ${EIGEN_EXT_TARGET})
	endif()

	#[[
	if(WIN32)
		if(${GLFW_SHARED})
			target_link_libraries(${EIGEN_TARGET} INTERFACE "${EIGEN_INSTALL_DIR}/lib/eigen.lib")
		else()
			target_link_libraries(${EIGEN_TARGET} INTERFACE "${EIGEN_INSTALL_DIR}/lib/eigen.lib")
		endif()
    endif()
    ]]#

	color_message("%greenOK (ExternalProject)\n")
	return()
endif()

color_message("%redNO METHOD\n")
color_message(FATAL_ERROR "No method defined to load Eigen.\n")