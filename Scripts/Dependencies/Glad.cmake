set(GLAD_TARGET "Glad")
set(GLAD_EXT_TARGET "dep_glad")

if(TARGET ${GLAD_TARGET})
	return()
endif()

color_message("Loading Glad... ")

# Options
option(GLAD_EXTERNALPROJECT "Obtain Glad from https://github.com/Dav1dde/glad.git" ON)
option(GLAD_SHARED "Build Glad as a shared library" OFF)

# Create target
add_library(${GLAD_TARGET} INTERFACE)

if(${GLAD_EXTERNALPROJECT})
	set(GLAD_INSTALL_DIR "${PROJECT_DEPENDENCY_DIR}/Glad")
	include(ExternalProject)
	ExternalProject_Add(
		${GLAD_EXT_TARGET}
		GIT_REPOSITORY "https://github.com/Dav1dde/glad.git"
		CMAKE_ARGS
            -DCMAKE_INSTALL_PREFIX=${GLAD_INSTALL_DIR}
            -DGLAD_INSTALL=ON
            -DGLAD_ALL_EXTENSIONS=ON
	)
	set_target_properties(${GLAD_EXT_TARGET} PROPERTIES FOLDER "Dependencies")

	# Create target
	target_include_directories(${GLAD_TARGET} INTERFACE "${GLAD_INSTALL_DIR}/include")
	if(NOT ${DEV_IGNORE_DEPENENDENCIES})
		add_dependencies(${GLAD_TARGET} ${GLAD_EXT_TARGET})
	endif()

	if(WIN32)
		if(${GLAD_SHARED})
			target_link_libraries(${GLAD_TARGET} INTERFACE "${GLAD_INSTALL_DIR}/lib/glad.lib")
		else()
			target_link_libraries(${GLAD_TARGET} INTERFACE "${GLAD_INSTALL_DIR}/lib/glad.lib")
		endif()
	endif()
	color_message("%greenOK (ExternalProject)\n")
	return()
endif()


color_message("%redNO METHOD\n")
color_message(FATAL_ERROR "No method defined to load Glad.\n")