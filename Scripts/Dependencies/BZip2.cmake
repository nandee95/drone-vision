set(BZIP2_TARGET "BZip2")
set(BZIP2_EXT_TARGET "dep_bzip2")

if(TARGET ${BZIP2_TARGET})
	return()
endif()

color_message("Loading BZip2... ")

# Options
option(BZIP2_EXTERNALPROJECT "Obtain BZip2 from https://github.com/sergiud/bzip2.git" ON)

# Create target
add_library(${BZIP2_TARGET} INTERFACE)

if(${BZIP2_EXTERNALPROJECT})

	set(BZIP2_INSTALL_DIR "${PROJECT_DEPENDENCY_DIR}/BZip2")

	include(ExternalProject)
	ExternalProject_Add(
		${BZIP2_EXT_TARGET}
		GIT_REPOSITORY "https://github.com/sergiud/bzip2.git"
		GIT_TAG cmake
		CMAKE_ARGS
            -DCMAKE_INSTALL_PREFIX=${BZIP2_INSTALL_DIR}
	)
	set_target_properties(${BZIP2_EXT_TARGET} PROPERTIES FOLDER "Dependencies")
	
	if(NOT ${DEV_IGNORE_DEPENENDENCIES})
		add_dependencies(${BZIP2_TARGET} ${BZIP2_EXT_TARGET})
	endif()
	target_include_directories(${BZIP2_TARGET} INTERFACE "${BZIP2_INSTALL_DIR}/include")
	if(WIN32)
		target_link_libraries(${BZIP2_TARGET} INTERFACE "${BZIP2_INSTALL_DIR}/lib/bz2$<$<CONFIG:DEBUG>:d>.lib")
	endif()
	color_message("%greenOK (ExternalProject)\n")
	return()
endif()

color_message("%redNO METHOD\n")
color_message(FATAL_ERROR "No method defined to load BZip2.\n")