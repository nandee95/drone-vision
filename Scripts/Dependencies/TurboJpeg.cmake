
set(TURBOJPEG_TARGET "TurboJpeg")
set(TURBOJPEG_EXT_TARGET "dep_turbojpeg")

if(TARGET ${TURBOJPEG_TARGET})
	return()
endif()

color_message("Loading TurboJpeg... ")

# Options
option(TURBOJPEG_EXTERNALPROJECT "Obtain TurboJpeg from github.com/libjpeg-turbo/libjpeg-turbo" ON)
option(TURBOJPEG_SHARED "Build TurboJpeg as a shared library" OFF)

# Create target
add_library(${TURBOJPEG_TARGET} INTERFACE)

if(${TURBOJPEG_EXTERNALPROJECT})
	set(TURBOJPEG_INSTALL_DIR "${PROJECT_DEPENDENCY_DIR}/TurboJpeg")
	include(ExternalProject)
	ExternalProject_Add(
		${TURBOJPEG_EXT_TARGET}
		GIT_REPOSITORY "https://github.com/libjpeg-turbo/libjpeg-turbo.git"
		CMAKE_ARGS
			-DCMAKE_INSTALL_PREFIX=${TURBOJPEG_INSTALL_DIR}
			-DREQUIRE_SIMD=ON
			-DWITH_TURBOJPEG=ON
			-DENABLE_SHARED=${TURBOJPEG_SHARED}
			-DENABLE_STATIC=$<NOT:$<BOOL:${TURBOJPEG_SHARED}>>
	)
	set_target_properties(${TURBOJPEG_EXT_TARGET} PROPERTIES FOLDER "Dependencies")

	# Create target
	target_include_directories(${TURBOJPEG_TARGET} INTERFACE "${TURBOJPEG_INSTALL_DIR}/include")
	if(NOT ${DEV_IGNORE_DEPENENDENCIES})
		add_dependencies(${TURBOJPEG_TARGET} ${TURBOJPEG_EXT_TARGET})
	endif()

	if(WIN32)
		if(${TURBOJPEG_SHARED})
			target_link_libraries(${TURBOJPEG_TARGET} INTERFACE "${TURBOJPEG_INSTALL_DIR}/lib/turbojpeg.lib")
		else()
			target_link_libraries(${TURBOJPEG_TARGET} INTERFACE "${TURBOJPEG_INSTALL_DIR}/lib/turbojpeg-static.lib")
		endif()
	endif()
	color_message("%greenOK (ExternalProject)\n")
	return()
endif()

color_message("%redNO METHOD\n")
color_message(FATAL_ERROR "No method defined to load TurboJpeg.\n")