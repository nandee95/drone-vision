set(NANOVG_EXT_TARGET "dep_nanovg")
set(NANOVG_TARGET "NanoVg")

if(TARGET ${NANOVG_TARGET})
	return()
endif()

color_message("Loading NanoVg... ")

# Options
option(NANOVG_EXTERNALPROJECT "Obtain NanoVg from https://github.com/memononen/nanovg.git" ON)

# Create target
add_library(${NANOVG_TARGET} INTERFACE)

# Load external project
if(${NANOVG_EXTERNALPROJECT})
	set(NANOVG_INSTALL_DIR "${PROJECT_DEPENDENCY_DIR}/NanoVg")
	include(ExternalProject)
	ExternalProject_Add(
		${NANOVG_EXT_TARGET}
		GIT_REPOSITORY "https://github.com/memononen/nanovg.git"
		PATCH_COMMAND ${CMAKE_COMMAND} -E copy_directory "${PROJECT_SCRIPTS_DIR}/Dependencies/Patch/NanoVg" "<SOURCE_DIR>/"
		CMAKE_ARGS
			-DCMAKE_INSTALL_PREFIX=${NANOVG_INSTALL_DIR}
	)
	set_target_properties(${NANOVG_EXT_TARGET} PROPERTIES FOLDER "Dependencies")
	ExternalProject_Get_Property(${NANOVG_EXT_TARGET} SOURCE_DIR)

	# Create target
	target_include_directories(${NANOVG_TARGET} INTERFACE ${NANOVG_INSTALL_DIR}/include/nanovg)
	if(NOT ${DEV_IGNORE_DEPENENDENCIES})
		add_dependencies(${NANOVG_TARGET} ${NANOVG_EXT_TARGET})
	endif()

	
	if(WIN32)
		if(${GLFW_SHARED})
			target_link_libraries(${NANOVG_TARGET} INTERFACE "${NANOVG_INSTALL_DIR}/lib/nanovg.lib")
		else()
			target_link_libraries(${NANOVG_TARGET} INTERFACE "${NANOVG_INSTALL_DIR}/lib/nanovg.lib")
		endif()
	endif()

	color_message("%greenOK (ExternalProject)\n")
	return()
endif()

color_message("%redNO METHOD\n")
color_message(FATAL_ERROR "No method defined to load NanoVg.\n")