set(GLFW_TARGET "Glfw")
set(GLFW_EXT_TARGET "dep_glfw")

if(TARGET ${GLFW_TARGET})
	return()
endif()

color_message("Loading Glfw... ")

# Options
option(GLFW_EXTERNALPROJECT "Obtain Glfw from github.com/glfw/glfw" ON)
option(GLFW_SHARED "Build Glfw as a shared library" OFF)

# Create target
add_library(${GLFW_TARGET} INTERFACE)

if(${GLFW_EXTERNALPROJECT})
	set(GLFW_INSTALL_DIR "${PROJECT_DEPENDENCY_DIR}/Glfw")
	include(ExternalProject)
	ExternalProject_Add(
		${GLFW_EXT_TARGET}
		GIT_REPOSITORY "https://github.com/glfw/glfw.git"
		CMAKE_ARGS
			-DCMAKE_INSTALL_PREFIX=${GLFW_INSTALL_DIR}
			-DGLFW_BUILD_TESTS=OFF
			-DGLFW_BUILD_DOCS=OFF
			-DGLFW_BUILD_EXAMPLES=OFF
			-DBUILD_SHARED_LIBS=${GLFW_SHARED}
	)
	set_target_properties(${GLFW_EXT_TARGET} PROPERTIES FOLDER "Dependencies")

	# Create target
	target_include_directories(${GLFW_TARGET} INTERFACE "${GLFW_INSTALL_DIR}/include")
	if(NOT ${DEV_IGNORE_DEPENENDENCIES})
		add_dependencies(${GLFW_TARGET} ${GLFW_EXT_TARGET})
	endif()

	if(WIN32)
		if(${GLFW_SHARED})
			target_link_libraries(${GLFW_TARGET} INTERFACE "${GLFW_INSTALL_DIR}/lib/glfw3dll.lib")
		else()
			target_link_libraries(${GLFW_TARGET} INTERFACE "${GLFW_INSTALL_DIR}/lib/glfw3.lib")
		endif()
	endif()
	color_message("%greenOK (ExternalProject)\n")
	return()
endif()


color_message("%redNO METHOD\n")
color_message(FATAL_ERROR "No method defined to load Glfw.\n")