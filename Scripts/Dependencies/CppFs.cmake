set(CPPFS_TARGET "CppFs")
set(CPPFS_EXT_TARGET "dep_cppfs")

if(TARGET ${CPPFS_TARGET})
	return()
endif()

color_message("Loading CppFs... ")

# Options
option(CPPFS_EXTERNALPROJECT "Obtain CppFs from github.com/cginternals/cppfs" ON)
option(CPPFS_SHARED "Build CppFs as a shared library" OFF)

# Create interface library
add_library(${CPPFS_TARGET} INTERFACE)

# Load external project
if(${CPPFS_EXTERNALPROJECT})
	set(CPPFS_INSTALL_DIR "${PROJECT_DEPENDENCY_DIR}/CppFs")
	include(ExternalProject)
	ExternalProject_Add(
		${CPPFS_EXT_TARGET}
		GIT_REPOSITORY "https://github.com/cginternals/cppfs.git"
		CMAKE_ARGS
			-DCMAKE_INSTALL_PREFIX=${CPPFS_INSTALL_DIR}
			-DOPTION_BUILD_TESTS=OFF
			-DBUILD_SHARED_LIBS=${CPPFS_SHARED}
	)
	if(NOT ${DEV_IGNORE_DEPENENDENCIES})
		add_dependencies(${CPPFS_TARGET} ${CPPFS_EXT_TARGET})
	endif()
	set_target_properties(${CPPFS_EXT_TARGET} PROPERTIES FOLDER "Dependencies")

	target_include_directories(${CPPFS_TARGET} INTERFACE "${CPPFS_INSTALL_DIR}/include")
	if(MSVC)
		target_link_libraries(${CPPFS_TARGET} INTERFACE "${CPPFS_INSTALL_DIR}/lib/cppfs$<$<CONFIG:DEBUG>:d>.lib")
		if(${CPPFS_SHARED})
			set(CPPFS_BINARY "${CPPFS_INSTALL_DIR}/cppfs$<$<CONFIG:DEBUG>:d>.dll")
		endif()
	elseif(MINGW)
		if(${CPPFS_SHARED})
			target_link_libraries(${CPPFS_TARGET} INTERFACE "${CPPFS_INSTALL_DIR}/lib/libcppfs.dll.a")
			set(CPPFS_BINARY "${CPPFS_INSTALL_DIR}/libcppfs.dll")
		else()
			target_link_libraries(${CPPFS_TARGET} INTERFACE "${CPPFS_INSTALL_DIR}/lib/libcppfs.a")
		endif()
	endif()

	color_message("%greenOK (ExternalProject)\n")
	return()
endif()

color_message("%redNO METHOD\n")
color_message(FATAL_ERROR "No method defined to load CppFs.\n")