set(LIBPNG_TARGET "LibPng")
set(LIBPNG_EXT_TARGET "dep_libpng")

if(TARGET ${LIBPNG_TARGET})
	return()
endif()

color_message("Loading LibPng... ")

# Options
option(LIBPNG_EXTERNALPROJECT "Obtain LibPng from https://github.com/glennrp/libpng" ON)
option(LIBPNG_SHARED "Build LibPng as a shared library" OFF)

# Create target
add_library(${LIBPNG_TARGET} INTERFACE)

if(${LIBPNG_EXTERNALPROJECT})
	set(LIBPNG_INSTALL_DIR "${PROJECT_DEPENDENCY_DIR}/LibPng")
	include(ExternalProject)
	ExternalProject_Add(
		${LIBPNG_EXT_TARGET}
		GIT_REPOSITORY "https://github.com/glennrp/libpng.git"
		CMAKE_ARGS
			-DCMAKE_INSTALL_PREFIX=${LIBPNG_INSTALL_DIR}
			-DPNG_TESTS=OFF
			-DPNG_EXECUTABLES=OFF
			-DLIBPNG_BUILD_EXAMPLES=OFF
			-DPNG_SHARED=${LIBPNG_SHARED}
			-DPNG_STATIC=$<NOT:$<BOOL:${LIBPNG_SHARED}>>
			-DZLIB_LIBRARY=${ZLIB_LIBRARIES}
			-DZLIB_INCLUDE_DIR=${ZLIB_INCLUDE_DIRS}
	)
	set_target_properties(${LIBPNG_EXT_TARGET} PROPERTIES FOLDER "Dependencies")

	# Create target
	target_include_directories(${LIBPNG_TARGET} INTERFACE "${LIBPNG_INSTALL_DIR}/include")
	if(NOT ${DEV_IGNORE_DEPENENDENCIES})
		add_dependencies(${LIBPNG_TARGET} ${LIBPNG_EXT_TARGET})
	endif()

	if(WIN32)
		if(${LIBPNG_SHARED})
			#set(LIBPNG_BINARIES "${LIBPNG_INSTALL_DIR}/bin/libpng3.dll" PARENT_SCOPE)
			target_link_libraries(${LIBPNG_TARGET} INTERFACE "${LIBPNG_INSTALL_DIR}/lib/libpng.lib")
		else()
			target_link_libraries(${LIBPNG_TARGET} INTERFACE "${LIBPNG_INSTALL_DIR}/lib/libpng16_staticd.lib")
		endif()
	endif()
	color_message("%greenOK (ExternalProject)\n")
	return()
endif()


color_message("%redNO METHOD\n")
color_message(FATAL_ERROR "No method defined to load LibPng.\n")