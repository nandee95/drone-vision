set(ZLIB_TARGET "Zlib")
set(ZLIB_EXT_TARGET "dep_zlib")

if(TARGET ${ZLIB_TARGET})
	return()
endif()

color_message("Loading Zlib... ")

# Options
option(ZLIB_EXTERNALPROJECT "Obtain Zlib from https://github.com/madler/zlib.git" ON)

# Create target
add_library(${ZLIB_TARGET} INTERFACE)

if(${ZLIB_EXTERNALPROJECT})

	set(ZLIB_INSTALL_DIR "${PROJECT_DEPENDENCY_DIR}/Zlib")
	include(ExternalProject)
	ExternalProject_Add(
		${ZLIB_EXT_TARGET}
		GIT_REPOSITORY "https://github.com/madler/zlib.git"
		CMAKE_ARGS
            -DCMAKE_INSTALL_PREFIX=${ZLIB_INSTALL_DIR}
            -L
	)
	set_target_properties(${ZLIB_EXT_TARGET} PROPERTIES FOLDER "Dependencies")
	
	set(ZLIB_INCLUDE_DIRS "${ZLIB_INSTALL_DIR}/include")
	set(ZLIB_LIBRARIES "${ZLIB_INSTALL_DIR}/lib/zlibstatic$<$<CONFIG:DEBUG>:d>.lib")

	if(NOT ${DEV_IGNORE_DEPENENDENCIES})
		add_dependencies(${ZLIB_TARGET} ${ZLIB_EXT_TARGET})
	endif()
	target_include_directories(${ZLIB_TARGET} INTERFACE "${ZLIB_INSTALL_DIR}/include")
	if(WIN32)
		target_link_libraries(${ZLIB_TARGET} INTERFACE "${ZLIB_INSTALL_DIR}/lib/zlibstatic$<$<CONFIG:DEBUG>:d>.lib")
	endif()
	color_message("%greenOK (ExternalProject)\n")
	return()
endif()

color_message("%redNO METHOD\n")
color_message(FATAL_ERROR "No method defined to load Zlib.\n")