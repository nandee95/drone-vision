set(RAPIDJSON_TARGET "RapidJson")
set(RAPIDJSON_EXT_TARGET "dep_rapidjson")

if(TARGET ${RAPIDJSON_TARGET})
	return()
endif()

color_message("Loading RapidJson... ")

# Options
option(RAPIDJSON_EXTERNALPROJECT "Obtain RapidJson from github.com/Tencent/rapidjson" ON)

# Create target
add_library(${RAPIDJSON_TARGET} INTERFACE)

if(${RAPIDJSON_EXTERNALPROJECT})
	set(RAPIDJSON_INSTALL_DIR "${PROJECT_DEPENDENCY_DIR}/RapidJson")
	include(ExternalProject)
	ExternalProject_Add(
		${RAPIDJSON_EXT_TARGET}
        GIT_REPOSITORY "https://github.com/Tencent/rapidjson.git"
		CMAKE_ARGS
            -DCMAKE_INSTALL_PREFIX=${RAPIDJSON_INSTALL_DIR}
            -DRAPIDJSON_BUILD_EXAMPLES=OFF
            -DRAPIDJSON_BUILD_TESTS=OFF
            -DRAPIDJSON_BUILD_DOC=OFF
            -DRAPIDJSON_BUILD_THIRDPARTY_GTEST=OFF
	)
	set_target_properties(${RAPIDJSON_EXT_TARGET} PROPERTIES FOLDER "Dependencies")

	# Create target
	target_include_directories(${RAPIDJSON_TARGET} INTERFACE "${RAPIDJSON_INSTALL_DIR}/include")
	if(NOT ${DEV_IGNORE_DEPENENDENCIES})
		add_dependencies(${RAPIDJSON_TARGET} ${RAPIDJSON_EXT_TARGET})
	endif()

	color_message("%greenOK (ExternalProject)\n")
	return()
endif()


color_message("%redNO METHOD\n")
color_message(FATAL_ERROR "No method defined to load RapidJson.\n")