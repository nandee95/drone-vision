
set(LIBSPNG_TARGET "Libspng")
set(LIBSPNG_EXT_TARGET "dep_libspng")

if(TARGET ${LIBSPNG_TARGET})
	return()
endif()

color_message("Loading Libspng... ")

# Options
option(LIBSPNG_EXTERNALPROJECT "Obtain Libspng from https://github.com/randy408/libspng" ON)
option(LIBSPNG_SHARED "Build Libspng as a shared library" ON)

# Create target
add_library(${LIBSPNG_TARGET} INTERFACE)

if(${LIBSPNG_EXTERNALPROJECT})
	set(LIBSPNG_INSTALL_DIR "${PROJECT_DEPENDENCY_DIR}/Libspng")
	include(ExternalProject)
	ExternalProject_Add(
		${LIBSPNG_EXT_TARGET}
		GIT_REPOSITORY "https://github.com/randy408/libspng.git"
		CMAKE_ARGS
			-DCMAKE_INSTALL_PREFIX=${LIBSPNG_INSTALL_DIR}
			-DSPNG_SHARED=${LIBSPNG_SHARED}
			-DSPNG_STATIC=$<NOT:$<BOOL:${LIBSPNG_SHARED}>>
			-DZLIB_LIBRARY=${ZLIB_LIBRARIES}
			-DZLIB_INCLUDE_DIR=${ZLIB_INCLUDE_DIRS}
	)
	set_target_properties(${LIBSPNG_EXT_TARGET} PROPERTIES FOLDER "Dependencies")

	# Create target
	target_include_directories(${LIBSPNG_TARGET} INTERFACE "${LIBSPNG_INSTALL_DIR}/include")
	if(NOT ${DEV_IGNORE_DEPENENDENCIES})
		add_dependencies(${LIBSPNG_TARGET} ${LIBSPNG_EXT_TARGET})
	endif()

	if(WIN32)
		if(${LIBSPNG_SHARED})
			target_link_libraries(${LIBSPNG_TARGET} INTERFACE "${LIBSPNG_INSTALL_DIR}/lib/spng.lib")
		else()
			target_link_libraries(${LIBSPNG_TARGET} INTERFACE "${LIBSPNG_INSTALL_DIR}/lib/spng_static.lib")
		endif()
	endif()
	color_message("%greenOK (ExternalProject)\n")
	return()
endif()

color_message("%redNO METHOD\n")
color_message(FATAL_ERROR "No method defined to load Libspng.\n")