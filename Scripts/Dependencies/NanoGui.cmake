set(NANOGUI_EXT_TARGET "dep_nanogui")
set(NANOGUI_TARGET "NanoGui")

if(TARGET ${NANOGUI_TARGET})
	return()
endif()

color_message("Loading NanoGui... ")

# Options
option(NANOGUI_EXTERNALPROJECT "Obtain NanoGui from https://github.com/wjakob/nanogui.git" ON)

# Create target
add_library(${NANOGUI_TARGET} INTERFACE)

# Load external project
if(${NANOGUI_EXTERNALPROJECT})
	set(NANOGUI_INSTALL_DIR "${PROJECT_DEPENDENCY_DIR}/NanoGui")
	include(ExternalProject)
	ExternalProject_Add(
		${NANOGUI_EXT_TARGET}
		GIT_REPOSITORY "https://github.com/wjakob/nanogui.git" 
		CMAKE_ARGS
			-DCMAKE_INSTALL_PREFIX=${NANOGUI_INSTALL_DIR}
			-DNANOGUI_BUILD_SHARED=OFF
			-DNANOGUI_BUILD_EXAMPLE=OFF
			-DNANOGUI_BUILD_PYTHON=OFF
	)
	set_target_properties(${NANOGUI_EXT_TARGET} PROPERTIES FOLDER "Dependencies")
	ExternalProject_Get_Property(${NANOGUI_EXT_TARGET} SOURCE_DIR)

	# Create target
	target_include_directories(${NANOGUI_TARGET} INTERFACE ${NANOGUI_INSTALL_DIR}/include)
	if(NOT ${DEV_IGNORE_DEPENENDENCIES})
		add_dependencies(${NANOGUI_TARGET} ${NANOGUI_EXT_TARGET})
	endif()

	
	if(WIN32)
		if(${GLFW_SHARED})
			target_link_libraries(${NANOGUI_TARGET} INTERFACE "${NANOGUI_INSTALL_DIR}/lib/nanogui.lib")
		else()
			target_link_libraries(${NANOGUI_TARGET} INTERFACE "${NANOGUI_INSTALL_DIR}/lib/nanogui.lib")
		endif()
	endif()

	color_message("%greenOK (ExternalProject)\n")
	return()
endif()

color_message("%redNO METHOD\n")
color_message(FATAL_ERROR "No method defined to load NanoGui.\n")