cmake_minimum_required(VERSION 3.17)

project(DroneVision)

# Set up directories
set(PROJECT_BUILD_DIR ${PROJECT_SOURCE_DIR}/Build)
set(PROJECT_SCRIPTS_DIR ${PROJECT_SOURCE_DIR}/Scripts)
set(PROJECT_DEPENDENCY_DIR ${PROJECT_SOURCE_DIR}/Dependencies)

# Load dependencies
include(${PROJECT_SCRIPTS_DIR}/ColorMessage.cmake)
include(${PROJECT_SCRIPTS_DIR}/Dependencies/Glm.cmake)
include(${PROJECT_SCRIPTS_DIR}/Dependencies/Glad.cmake)
include(${PROJECT_SCRIPTS_DIR}/Dependencies/Glfw.cmake)
include(${PROJECT_SCRIPTS_DIR}/Dependencies/CppFs.cmake)
include(${PROJECT_SCRIPTS_DIR}/Dependencies/OpenGL.cmake)
include(${PROJECT_SCRIPTS_DIR}/Dependencies/Zlib.cmake)
include(${PROJECT_SCRIPTS_DIR}/Dependencies/LibPng.cmake)
include(${PROJECT_SCRIPTS_DIR}/Dependencies/BZip2.cmake)
include(${PROJECT_SCRIPTS_DIR}/Dependencies/NanoVg.cmake)
include(${PROJECT_SCRIPTS_DIR}/Dependencies/NanoGui.cmake)
include(${PROJECT_SCRIPTS_DIR}/Dependencies/Eigen.cmake)
include(${PROJECT_SCRIPTS_DIR}/Dependencies/RapidJson.cmake)
include(${PROJECT_SCRIPTS_DIR}/Dependencies/TurboJpeg.cmake)
include(${PROJECT_SCRIPTS_DIR}/Dependencies/Libspng.cmake)

# Set up executable
set(APP_TARGET "DroneVision")
set(APP_SOURCE_DIR ${PROJECT_SOURCE_DIR}/Source)

file(GLOB_RECURSE APP_SOURCE ${APP_SOURCE_DIR}/*.[ch]pp)
add_executable(${APP_TARGET} ${APP_SOURCE})

target_compile_features(${APP_TARGET} PRIVATE cxx_std_17)
target_link_libraries(${APP_TARGET} OpenGL::GL Glm Glad Glfw CppFs Zlib LibPng BZip2 Eigen NanoVg NanoGui RapidJson TurboJpeg Libspng)