#include "Person.hpp"

ObjMesh Person::mesh;
int32_t Person::count;

void Person::Init()
{
    mesh.Create("../../Resources/Person.obj");
}

void Person::Draw(const DrawInfo& info)
{
    // Render object
    mesh.SetPosition(GetPosition() + glm::vec3(0, GetScale().y / 2, 0));
    mesh.SetColor(GetColor());
    mesh.SetScale(GetScale());

    if (outline)
    {
        glEnable(GL_STENCIL_TEST);
        glClear(GL_STENCIL_BUFFER_BIT);

        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
        glStencilFunc(GL_ALWAYS, 1, 0xFF);
        glStencilMask(0xFF);
    }
    mesh.Draw(info);

    if (outline)
    {
        glLineWidth(3);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
        glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
        glStencilMask(0x00);
        mesh.SetColor({ 1,1,1,0.95 });
        mesh.Draw(info);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glLineWidth(1);
        glDisable(GL_STENCIL_TEST);
    }
}
