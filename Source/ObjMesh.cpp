#include "ObjMesh.hpp"
#include <glm/gtx/quaternion.hpp>

Shader ObjMesh::shader;

void ObjMesh::Init()
{
	shader.Create("../../Shaders/Object.vert", "../../Shaders/Object.frag");
}

void ObjMesh::Create(const char* filename)
{
	// Borrowed from: http://www.opengl-tutorial.org/hu/beginners-tutorials/tutorial-7-model-loading/
	FILE* file = fopen(filename, "r");
	if (file == NULL) {
		throw Exception(E::FailedToOpenFile, "Failed to open object from: ", filename);
	}
	std::vector< unsigned int > vertexIndices;
	std::vector< glm::vec3 > temp_vertices;

    //Load file
	while (true)
	{
		char lineHeader[128];
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break;

		if (strcmp(lineHeader, "v") == 0) {
			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);
		}
		else if (strcmp(lineHeader, "f") == 0) {
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
			if (matches != 9) {
				throw Exception(E::FailedToOpenFile, "Failed to parse file: ", filename);
			}
			vertexIndices.push_back(vertexIndex[0] - 1);
			vertexIndices.push_back(vertexIndex[1] - 1);
			vertexIndices.push_back(vertexIndex[2] - 1);
		}
	}

    fclose(file);

    //Generate mesh
	struct Vertex
	{
		glm::vec3 pos, norm;
	};
	std::vector< Vertex> out_vertices;
	for (unsigned int i = 0; i < vertexIndices.size(); i += 3) {

        // Calculate normals
		const auto normal = glm::triangleNormal(temp_vertices[vertexIndices[i]], temp_vertices[vertexIndices[i + 1]], temp_vertices[vertexIndices[i + 2]]);
		out_vertices.push_back({ temp_vertices[vertexIndices[i]], normal });
		out_vertices.push_back({ temp_vertices[vertexIndices[i + 1]], normal });
		out_vertices.push_back({ temp_vertices[vertexIndices[i + 2]], normal });
	}
	va.Create();
	va.Bind();
	va.FillVertices(out_vertices);
	va.SetAttribPointers({ { 3,GL_FLOAT },{ 3,GL_FLOAT } });
	count = out_vertices.size();

}

void ObjMesh::Draw(const DrawInfo& info)
{
	UpdateModelMatrix();
	shader.Use();
	shader.SetUniform(0, model);
	shader.SetUniform(1, GetColor());
	shader.SetUniform(2, glm::mat4_cast(GetOrientation()));
	va.Bind();
	va.DrawArrays(GL_TRIANGLES, 0, count);
}
