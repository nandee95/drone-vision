#pragma once

#include <nanogui/nanogui.h>

#include "GuiWindow.hpp"

class ParserWindow : public GuiWindow
{
public:
    nanogui::Window* window;
    nanogui::Label* label;
    nanogui::ProgressBar* slider;
    nanogui::Button* cancelButton;

    void Create(nanogui::Screen* screen)
    {
        window = new nanogui::Window(screen, (std::string)"Parsing ???...");
        window->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Vertical, nanogui::Alignment::Minimum, 6, 6));
        window->center();

        label = new nanogui::Label(window, "Parsing header...");
        label->setFixedWidth(300);

        slider = new nanogui::ProgressBar(window);
        slider->setFixedWidth(300);

        cancelButton = new nanogui::Button(window, "Cancel");
        window->setVisible(false);
    }

    void SetFile(const std::string& file)
    {
        std::string name = file.substr(std::min(file.find_last_of("/"), file.find_last_of("\\")) + 1);
        window->setTitle((std::string)"Parsing " + name + "...");
        window->center();
    }

    virtual void SetState(const GuiState& state)
    {
        window->setVisible(state == GuiState::Parsing);
    }
    virtual void Resize(const glm::vec2& resolution) override
    {
        window->center();
    }

};