#pragma once
#include <vector>
#include <functional>
#include <string>
#include <algorithm>
#include <emmintrin.h>

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtx/closest_point.hpp>

enum class OrientAlgo
{
	None,
	DominantAngle,
	PrincipalComponent
};

struct SegmentGroup
{
	std::string name;

	//Color
	glm::vec<3, uint8_t, glm::defaultp> color{ 255,0,0 };

	// Post
	OrientAlgo orientAlgo = OrientAlgo::None;

	uint32_t GetSingleColorMask() const
	{
		return 0xFF << 24 | color.b << 16 | color.g << 8 | color.r;
	}

	__m128i GetColorMask() const
	{
		__m128i mask;
		mask.m128i_u32[0] = GetSingleColorMask();
		mask.m128i_u32[1] = mask.m128i_u32[0];
		mask.m128i_u32[2] = mask.m128i_u32[0];
		mask.m128i_u32[3] = mask.m128i_u32[0];
		return mask;
	}
};

struct Blob
{
	SegmentGroup* group;
	union
	{
		glm::ivec2 position;
		glm::ivec2 AA;
	};
	glm::ivec2 BB;

	int32_t angle = 0;

	glm::ivec2 size;
	uint64_t area;
	bool touchesEdge = false;
	glm::vec<2, uint64_t> midPoint;
	uint32_t pixels = 0;

	bool CollidesWith(const Blob& other)
	{
		return (position.x + size.x >= other.position.x && other.position.x + other.size.x >= position.x) &&
			(position.y + size.y >= other.position.y && other.position.y + other.size.y >= position.y);
	}

	template<bool Extend = false>
	bool InRange(const glm::ivec2& point, const int32_t& range) const
	{
		return (AA.x - range <= point.x && BB.x + (Extend ? range + 4 : range) >= point.x) && (AA.y - range <= point.y && BB.y + range >= point.y);
	}

	glm::ivec2 GetEdgePoint(const int32_t& at)
	{
		if (at <= 16)
			return AA + glm::ivec2{ (at * size.x) / 16 ,0 };
		return AA + glm::ivec2{ size.x, (at - 16) * size.y / 16 };
	}
};



class BlobDetector
{
protected:
	std::vector<SegmentGroup> groups;
public:
	BlobDetector()
	{
	}

	void AddSegmentGroup(const SegmentGroup& sg)
	{
		groups.push_back(sg);
	}

	void ClearSegmentGroups()
	{
		groups.clear();
	}

	void getLine(const float& x1, const float& y1, const float& x2, const float& y2, float& a, float& b, float& c)
	{
		a = y1 - y2;
		b = x2 - x1;
		c = x1 * y2 - x2 * y1;
	}

	float dist(const float& pct1X, const float& pct1Y, const float& pct2X, const float& pct2Y, const float& pct3X, const float& pct3Y)
	{
		static float a, b, c;
		getLine(pct2X, pct2Y, pct3X, pct3Y, a, b, c);
		return abs(a * pct1X + b * pct1Y + c) / sqrt(a * a + b * b);
	}


	std::vector<Blob> Detect(const char* image, const glm::ivec2& resolution)
	{
		std::vector<Blob> result;

		char* first = const_cast<char*>(image);

		SegmentGroup* currentGroup = nullptr;
		__m128i lastColor{};
		auto found = result.end();
		for (int32_t y = 0; y < resolution.y; ++y)
		{
			for (int32_t x = 0; x < resolution.x; x += 4)
			{
				// Checking if the row of pixels are changed
				if (_mm_cmpeq_epi32(*reinterpret_cast<__m128i*>(first), lastColor).m128i_u32[0]) // checking 4 pixel at a time
				{
					if (currentGroup)
					{
						EvaluatePixel<true>(result, currentGroup, { x + 3,y });
					}
				}
				else // Change in the image
				{
					lastColor = *reinterpret_cast<__m128i*>(first);
					//Try to find a group
					currentGroup = nullptr;
					for (uint32_t offset : {0, 32, 64, 96})
					{
						for (auto& g : groups)
						{
							if (*reinterpret_cast<uint32_t*>(first + offset) == g.GetSingleColorMask()) // If the pixel is from a group
							{
								EvaluatePixel(result, &g, { x,y });
								currentGroup = &g; // Store the group
							}
						}
					}
				}

				first += 16;
			}
		}

		// Post processing
		for (auto it = result.begin(); it != result.end();)
		{
			// Calculate size and area
			it->size = it->BB - it->AA;
			it->area = static_cast<uint64_t>(it->size.x) * static_cast<uint64_t>(it->size.y);

			if (it->area < 100) // Remove too small blobs
			{
				it = result.erase(it);
				continue;
			}

			// Calculate avaraged mid point
			if (it->pixels > 0)
				it->midPoint /= it->pixels;

			// Check if blob is too close to the edge
			it->touchesEdge = it->AA.x < 8 || it->AA.y < 8 || it->BB.x > resolution.x - 8 || it->BB.y > resolution.y - 8;

			// Estimate orientation
			if (it->group->orientAlgo == OrientAlgo::DominantAngle)
			{
				auto mask = it->group->GetSingleColorMask();
				int32_t distribution[180]{ 0 };
				for (int32_t y = it->AA.y; y < std::min(it->BB.y, resolution.y); y++)
				{
					for (int32_t x = it->AA.x; x < std::min<int32_t>(it->BB.x, resolution.x); x++)
					{

						if (*reinterpret_cast<const uint32_t*>(image + 4 * (y * resolution.x + x)) != mask)
							continue;

						glm::ivec2 diff = glm::ivec2{ x,y } - glm::ivec2(it->midPoint.x, it->midPoint.y);
						if (diff.x == 0) continue; // zero div
						float angle = std::atan2f(diff.y, diff.x) / glm::two_pi<float>() + 0.5f;
						distribution[static_cast<int32_t>(angle * 360.f) % 180]++;
					}
				}

				for (int32_t i = 0; i < 180; i++) //Find the angle with the largest number of pixels
				{
					if (distribution[i] > distribution[it->angle]) it->angle = i;

				}
			}
			else if (it->group->orientAlgo == OrientAlgo::PrincipalComponent)
			{
				auto mask = it->group->GetSingleColorMask();
				float step = glm::pi<float>();

				uint64_t sse = 0, sse2 = 0;
				float angle = 0; // in radians
				constexpr const float targetStep = glm::radians(1.f);
				while (step > targetStep)
				{
					sse = sse2 = 0; // Sum sqared error
					for (int32_t y = it->AA.y; y < std::min(it->BB.y, resolution.y); y++)
					{
						for (int32_t x = it->AA.x; x < std::min<int32_t>(it->BB.x, resolution.x); x++)
						{
							if (*reinterpret_cast<const uint32_t*>(image + 4 * (y * resolution.x + x)) != mask)
								continue;

							const glm::vec2 a = static_cast<glm::vec2>(it->midPoint);
							const glm::vec2 b = a + glm::vec2(cosf(angle), sinf(angle)) * 10.f;
							const glm::vec2 b2 = a + glm::vec2(cosf(angle + step), sinf(angle + step)) * 10.f;
							const float d1 = dist(x, y, a.x, a.y, b.x, b.y);
							const float d2 = dist(x, y, a.x, a.y, b2.x, b2.y);
							sse += d1 * d1;
							sse2 += d2 * d2;


						}
					}

					if (sse2 < sse) angle += step;
					else angle -= step / 2.f;

					step /= 2.f;
				}

				it->angle = glm::degrees(angle);
			}

			it++;
		}



		return std::move(result);
	}

	template<bool Extend = false>
	static void EvaluatePixel(std::vector<Blob>& result, SegmentGroup* currentGroup, const glm::ivec2& pixel)
	{
		static std::vector<Blob>::iterator found;
		found = result.end();
		for (auto it = result.begin(); it != result.end();)
		{
			if (it->group != currentGroup)
			{
				++it;
				continue;
			}

			if (it->InRange<Extend>(pixel, 4))
			{
				if (found == result.end()) // Extend blobs
				{
					it->AA.x = std::min<int32_t>(pixel.x, it->AA.x);
					it->AA.y = std::min<int32_t>(pixel.y, it->AA.y);
					it->BB.x = std::max<int32_t>(pixel.x, it->BB.x);
					it->BB.y = std::max<int32_t>(pixel.y, it->BB.y);
					if constexpr (Extend)
					{

						it->midPoint += pixel * 4 - glm::ivec2(7, 7);
						it->pixels += 4;
					}
					else
					{
						it->midPoint += pixel;
						it->pixels++;
					}

					found = it;
				}
				else // Combine blobs
				{
					found->AA.x = std::min<int32_t>(found->AA.x, it->AA.x);
					found->AA.y = std::min<int32_t>(found->AA.y, it->AA.y);
					found->BB.x = std::max<int32_t>(found->BB.x, it->BB.x);
					found->BB.y = std::max<int32_t>(found->BB.y, it->BB.y);
					it = result.erase(it);
					continue;
				}
			}
			++it;
		}

		// Create blob
		if (found == result.end())
		{
			result.push_back({});
			auto& blob = result.back();
			blob.AA = pixel;
			blob.BB = pixel + glm::ivec2{ 1,1 };
			blob.group = currentGroup;
		}
	}
};