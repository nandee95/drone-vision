#include "CoordinateSystem.hpp"

void CoordinateSystem::Create()
{
	mesh.Create("../../Resources/Axis.obj");
	mesh.SetScale({ 2.0,2.0,2.0 });
}

void CoordinateSystem::Draw(const DrawInfo& info)
{
	mesh.SetOrientation({ 1,0,0,0 });
	mesh.SetColor({ 1,0,0,1 });
	mesh.Draw(info);
	mesh.SetOrientation({ 0.707,0,0,0.707 });
	mesh.SetColor({ 0,1,0,1 });
	mesh.Draw(info);
	mesh.SetOrientation({ 0.707,0,-0.707,0 });
	mesh.SetColor({ 0,0,1,1 });
	mesh.Draw(info);
}
