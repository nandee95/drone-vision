#include "Dataset.hpp"
#include <fstream>
#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>

void Dataset::Load(const char* filename, std::function<bool(std::string, int, int)> progressCallback)
{
    json::Document doc;
    std::ifstream jsonIfs(filename);
    if (!jsonIfs) return;

    json::IStreamWrapper wrapper(jsonIfs);
    doc.ParseStream(wrapper);

    try {
        if (doc.IsObject())
        {
            if (!progressCallback("camera", 0, 3)) return;
            if (doc.HasMember("camera") && doc["camera"].IsObject())
            {
                auto& camera = doc["camera"].GetObject();
                if (camera.HasMember("width") && camera["width"].IsInt())
                {
                    cam.resolution.x = camera["width"].GetInt();
                    if (cam.resolution.x < 0 || cam.resolution.x > 5000)
                    {
                        throw Exception(E::FailedToParseFile, "camera.width is out of range");
                    }
                }
                else
                {
                    throw Exception(E::FailedToParseFile, "camera.width is missing or wrong type");
                }

                if (camera.HasMember("frameRate") && camera["frameRate"].IsInt())
                {
                    cam.frameRate = camera["frameRate"].GetInt();
                    if (cam.frameRate <= 0)
                    {
                        throw Exception(E::FailedToParseFile, "camera.frameRate is out of range");
                    }
                }

                if (!progressCallback("camera", 1, 3)) return;
                if (camera.HasMember("height") && camera["height"].IsInt())
                {
                    cam.resolution.y = camera["height"].GetInt();
                    if (cam.resolution.y < 0 || cam.resolution.y > 5000)
                    {
                        throw Exception(E::FailedToParseFile, "camera.height is out of range");
                    }
                }
                else
                {
                    throw Exception(E::FailedToParseFile, "camera.height is missing or wrong type");
                }
                if (!progressCallback("camera", 2, 3)) return;
                if (camera.HasMember("fov") && camera["fov"].IsInt())
                {
                    cam.fov = camera["fov"].GetInt();
                    if (cam.fov < 30 || cam.fov > 120)
                    {
                        throw Exception(E::FailedToParseFile, "camera.fov is out of range");
                    }
                }
                else
                {
                    throw Exception(E::FailedToParseFile, "camera.fov is missing or wrong type");
                }
                if (!progressCallback("camera", 3, 3)) return;
            }
            else
            {
                throw Exception(E::FailedToParseFile, "camera is missing or wrong type");
            }

            if (doc.HasMember("groups") && doc["groups"].IsArray())
            {
                auto& groups = doc["groups"].GetArray();
                int count = 0;
                if (!progressCallback("groups", 0, groups.Size())) return;

                for (auto& g : groups)
                {
                    if (g.IsObject())
                    {
                        std::string name;
                        if (g.HasMember("name") && g["name"].IsString())
                            name = g["name"].GetString();
                        else
                            throw Exception(E::FailedToParseFile, "groups[", count, "].name is missing or wrong type");


                        segments.insert({name,{}});
                        auto& segment = segments.at(name);
                    	
                        if (g.HasMember("color") && g["color"].IsArray() && g["color"].GetArray().Size() == 3)
                        {
                            int cc = 0;
                            for (auto& c : g["color"].GetArray())
                            {
                                if (c.IsFloat())
                                {
                                    segment.color[cc] = c.GetFloat();
                                }
                                else if (g["color"][cc].IsInt())
                                {
                                    segment.color[cc] = c.GetInt();
                                }
                                else
                                {
                                    throw Exception(E::FailedToParseFile, "groups[", count, "].color[", cc, "] is wrong type");
                                }
                                cc++;
                            }
                        }
                        else
                            throw Exception(E::FailedToParseFile, "groups[", count, "].color is missing or wrong type");


                        if (g.HasMember("renderer") && g["renderer"].IsObject())
                        {
                            auto& renderer = g["renderer"].GetObject();

                            if (renderer.HasMember("type") && renderer["type"].IsString())
                            {
                                std::string type = renderer["type"].GetString();
                                if (type == "box") segment.renderer.type = SegmentType::Box;
                                else if (type == "human" || type == "person") segment.renderer.type = SegmentType::Person;
                                else  throw Exception(E::FailedToParseFile, "groups[", count, "].renderer.type invalid enum");
                            }
                            else  throw Exception(E::FailedToParseFile, "groups[", count, "].renderer.type is missing");

                            if (segment.renderer.type == SegmentType::Box)
                            {
                                if (renderer.HasMember("size") && renderer["size"].IsArray() && renderer["size"].GetArray().Size() == 3)
                                {
                                    auto& size = renderer["size"];
                                    int pc = 0;
                                    for (auto& p : size.GetArray())
                                    {
                                        if (p.IsFloat())
                                        {
                                            segment.renderer.size[pc] = p.GetFloat();
                                        }
                                        else if (p.IsInt())
                                        {
                                            segment.renderer.size[pc] = p.GetInt();
                                        }
                                        else
                                        {
                                            throw Exception(E::FailedToParseFile, "groups[", count, "].renderer.size[", pc, "] is missing or wrong type");
                                        }
                                        pc++;
                                    }
                                }
                                else  throw Exception(E::FailedToParseFile, "groups[", count, "].renderer.size is missing or invalid type");
                            }
                            else
                            {
                                if (renderer.HasMember("height") && renderer["height"].IsInt())
                                {
                                    segment.renderer.height = renderer["height"].GetInt();
                                }
                                else if (renderer.HasMember("height") && renderer["height"].IsFloat())
                                {
                                    segment.renderer.height = renderer["height"].GetFloat();
                                }
                                else  throw Exception(E::FailedToParseFile, "groups[", count, "].renderer.height is missing or invalid type");
                            }
                        }


                        if (segment.renderer.type != SegmentType::None)
                        {
                            if (g.HasMember("blobDetector") && g["blobDetector"].IsObject())
                            {
                                auto blobDetector = g["blobDetector"].GetObject();

                                if (blobDetector.HasMember("avgHeight") && blobDetector["avgHeight"].IsInt())
                                    segment.details.avgHeight = blobDetector["avgHeight"].GetInt();
                                else if (blobDetector.HasMember("avgHeight") && blobDetector["avgHeight"].IsFloat())
                                    segment.details.avgHeight = blobDetector["avgHeight"].GetFloat();
                                else
                                    throw Exception(E::FailedToParseFile, "groups[", count, "].blobDetector.avgHeight is missing or wrong type");


                                if (blobDetector.HasMember("minArea") && blobDetector["minArea"].IsInt())
                                    segment.details.minArea = blobDetector["minArea"].GetInt();
                                else if (blobDetector.HasMember("minArea") && blobDetector["minArea"].IsFloat())
                                    segment.details.minArea = blobDetector["minArea"].GetFloat();
                                else
                                    throw Exception(E::FailedToParseFile, "groups[", count, "].blobDetector.minArea is missing or wrong type");

                                if (blobDetector.HasMember("maxArea") && blobDetector["maxArea"].IsInt())
                                    segment.details.maxArea = blobDetector["maxArea"].GetInt();
                                else if (blobDetector.HasMember("maxArea") && blobDetector["maxArea"].IsFloat())
                                    segment.details.maxArea = blobDetector["maxArea"].GetFloat();
                                else
                                    throw Exception(E::FailedToParseFile, "groups[", count, "].blobDetector.maxArea is missing or wrong type");


                                if (blobDetector.HasMember("minDistance") && blobDetector["minDistance"].IsInt())
                                    segment.details.minDistance = blobDetector["minDistance"].GetInt();
                                else if (blobDetector.HasMember("minDistance") && blobDetector["minDistance"].IsFloat())
                                    segment.details.minDistance = blobDetector["minDistance"].GetFloat();
                                else
                                    throw Exception(E::FailedToParseFile, "groups[", count, "].blobDetector.minDistance is missing or wrong type");


                                if (blobDetector.HasMember("maxDistance") && blobDetector["maxDistance"].IsInt())
                                    segment.details.maxDistance = blobDetector["maxDistance"].GetInt();
                                else if (blobDetector.HasMember("maxDistance") && blobDetector["maxDistance"].IsFloat())
                                    segment.details.maxDistance = blobDetector["maxDistance"].GetFloat();
                                else
                                    throw Exception(E::FailedToParseFile, "groups[", count, "].blobDetector.maxDistance is missing or wrong type");

                            }
                            else
                                throw Exception(E::FailedToParseFile, "groups[", count, "].blobDetector is missing or wrong type");
                        }
                    }
                    else
                    {
                        throw Exception(E::FailedToParseFile, "groups[", count, "] is not an object");
                    }
                    count++;
                    if (!progressCallback("groups", count, groups.Size())) return;
                }
            }
            else
                throw Exception(E::FailedToParseFile, "groups is missing or wrong type");

            if (doc.HasMember("frames") && doc["frames"].IsArray())
            {
                auto& framesArray = doc["frames"].GetArray();
                if (!progressCallback("frames", 0, framesArray.Size())) return;
                int count = 0;
                for (auto& f : framesArray)
                {
                    if (f.IsObject())
                    {
                        frames.push_back({});
                        auto& frame = frames.back();
                        if (f.HasMember("position") && f["position"].IsArray() && f["position"].GetArray().Size() == 3)
                        {
                            int pc = 0;
                            for (auto& p : f["position"].GetArray())
                            {
                                if (p.IsFloat())
                                {
                                    frame.position[pc] = p.GetFloat();
                                }
                                else if (f["position"][pc].IsInt())
                                {
                                    frame.position[pc] = p.GetInt();
                                }
                                else
                                {
                                    throw Exception(E::FailedToParseFile, "frames[", count, "].position[", pc, "] is missing or wrong type");
                                }
                                pc++;
                            }
                        }


                        if (f.HasMember("orientation") && f["orientation"].IsArray() && f["orientation"].GetArray().Size() == 3)
                        {
                            int oc = 0;
                            for (auto& o : f["orientation"].GetArray())
                            {
                                if (o.IsFloat())
                                {
                                    frame.orientation[oc] = o.GetFloat();
                                }
                                else if (f["orientation"][oc].IsInt())
                                {
                                    frame.orientation[oc] = o.GetInt();
                                }
                                else
                                {
                                    throw Exception(E::FailedToParseFile, "frames[", count, "].orientation[", oc, "] is missing or wrong type");
                                }
                                oc++;
                            }
                        }


                        if (f.HasMember("elapsedTime") && f["elapsedTime"].IsFloat())
                            frame.timestamp = f["elapsedTime"].GetFloat();
                        else
                            throw Exception(E::FailedToParseFile, "frames[", count, "].elapsedTime is missing or wrong type");

                        if (f.HasMember("imageFrame") && f["imageFrame"].IsString())
                            frame.rgbImage = f["imageFrame"].GetString();
                        else
                            throw Exception(E::FailedToParseFile, "frames[", count, "].imageFrame is missing or wrong type");

                        if (f.HasMember("imageSegmented") && f["imageSegmented"].IsString())
                            frame.segImage = f["imageSegmented"].GetString();
                        else
                            throw Exception(E::FailedToParseFile, "frames[", count, "].imageSegmented is missing or wrong type");
                    }
                    else
                    {
                        throw Exception(E::FailedToParseFile, "frames[", count, "] is not an object");
                    }

                    count++;
                    if (!progressCallback("frames", count, framesArray.Size())) return;
                }
            }
            else
            {
                throw Exception(E::FailedToParseFile, "frames is missing or wrong type");
            }
        }
        else
        {
            throw Exception(E::FailedToParseFile, "root object is missing");
        }
    }
    catch (Exception& e)
    {
        Reset();
        throw e;
    }

    loaded = true;
}

void Dataset::Reset()
{
    frames.clear();
    segments.clear();
    loaded = false;
}
