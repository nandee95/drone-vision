#include "Grid.hpp"

#include <glad/glad.h>
#include <vector>
#include <glm/vec3.hpp>

Grid::Grid()
{
    // Load shader
    shader.Create("../../Shaders/Grid.vert", "../../Shaders/Grid.frag");

    // Generate vertices
    std::vector<glm::vec3> vertices;
    float size = 1000;
    for (int32_t x = -300; x <= 300; x++)
    {
        vertices.push_back({ x * size,0,-100 * size });
        vertices.push_back({ x * size,0,100 * size });
        vertices.push_back({ -100 * size,0,x * size });
        vertices.push_back({ 100 * size,0,x * size });
    }
    count = vertices.size();

    // Create buffer
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), vertices.data(), GL_STATIC_DRAW);

    // Create vertex array
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    // Unbind buffers
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void Grid::Draw(const DrawInfo& info)
{
    shader.Use();
    shader.SetUniform(0, info.PV);
    glBindVertexArray(VAO);
    glDrawArrays(GL_LINES, 0, count);
}
