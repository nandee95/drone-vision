#pragma once

enum class GuiState
{
    Default,
    Playing,
    Parsing,
    Paused,
    Stopped
};

class GuiWindow
{
public:
    virtual void SetState(const GuiState& state)=0;
    virtual void Resize(const glm::vec2& resolution) = 0;
};