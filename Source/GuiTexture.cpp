#include "GuiTexture.hpp"
#include "Exception.hpp"

#include <fstream>
#include <algorithm>

tjhandle GuiTexture::jpegHandle = tjInitDecompress();

void GuiTexture::Create()
{
    glGenTextures(1, &texture);

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1280, 720, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
}

void GuiTexture::SetImage(const std::string& filename)
{
    auto pos = filename.find_last_of('.');
    if (pos == std::string::npos)
    {
        throw Exception(E::FailedToOpenFile, "No extension found: ", filename);
    }

    auto extension = filename.substr(pos + 1);

    std::transform(extension.begin(), extension.end(), extension.begin(),
        [](unsigned char c) { return std::tolower(c); });

    if (extension == "jpeg" || extension == "jpg")
    {
        std::ifstream ifs(filename, std::ios::ate | std::ios::binary);
        if (!ifs) throw Exception(E::FailedToOpenFile, "Failed to open: ", filename);

        auto size = ifs.tellg();
        ifs.seekg(std::ios::beg);
        size -= ifs.tellg();

        std::vector<char> jpegBuffer(size);
        ifs.read(jpegBuffer.data(), jpegBuffer.size());
        ifs.close();
        int w, h, ss, cs;
        if (tjDecompressHeader3(jpegHandle, reinterpret_cast<unsigned char*>(jpegBuffer.data()), jpegBuffer.size(), &w, &h, &ss, &cs) != 0)
        {
            throw Exception(E::FailedToOpenFile, "File corrupted: ", filename,", ",tjGetErrorStr2(jpegHandle));
        }

        int n;

        std::vector<char> tempBuf(w * h * 4);
        if (tjDecompress2(jpegHandle, reinterpret_cast<unsigned char*>(jpegBuffer.data()), jpegBuffer.size(), reinterpret_cast<unsigned char*>(tempBuf.data()), w, w * 4, h, TJPF_RGBA, TJFLAG_FASTDCT))
        {
            throw Exception(E::FailedToOpenFile, "File corrupted: ", filename, ", ", tjGetErrorStr2(jpegHandle));
        }

        bufferMutex.lock();
        rgbBuffer.resize(tempBuf.size());
        memcpy(rgbBuffer.data(), tempBuf.data(), rgbBuffer.size());
        bufferMutex.unlock();

        resolution.x = w;
        resolution.y = h;

    }
    else if (extension == "png")
    {
        std::ifstream ifs(filename, std::ios::ate | std::ios::binary);
        if (!ifs) throw Exception(E::FailedToOpenFile, "Failed to open: ", filename);

        auto size = ifs.tellg();
        ifs.seekg(std::ios::beg);
        size -= ifs.tellg();

        std::vector<char> pngBuffer(size);
        ifs.read(pngBuffer.data(), pngBuffer.size());
        ifs.close();

        spng_ctx* pngHandle = spng_ctx_new(0);
        if (spng_set_png_buffer(pngHandle, pngBuffer.data(), pngBuffer.size()))
        {
            spng_ctx_free(pngHandle);
            throw Exception(E::FailedToOpenFile, "File corrupted: ", filename);
        }


        size_t len;
        if (spng_decoded_image_size(pngHandle, SPNG_FMT_RGBA8, &len))
        {
            spng_ctx_free(pngHandle);
            throw Exception(E::FailedToOpenFile, "File corrupted: ", filename);
        }
        spng_ihdr ihdr;
        spng_get_ihdr(pngHandle, &ihdr);

        std::vector<char> tempBuf(ihdr.width * ihdr.height * 4);
        if (spng_decode_image(pngHandle, tempBuf.data(), len, SPNG_FMT_RGBA8, 0))
        {
            spng_ctx_free(pngHandle);
            throw Exception(E::FailedToOpenFile, "File corrupted: ", filename);
        }

        bufferMutex.lock();
        rgbBuffer.resize(tempBuf.size());
        memcpy(rgbBuffer.data(), tempBuf.data(), rgbBuffer.size());
        bufferMutex.unlock();

        spng_ctx_free(pngHandle);

        resolution.x = ihdr.width;
        resolution.y = ihdr.height;
        //FlipImage(rgbBuffer.data(), resolution.x * 4, resolution.y);

    }
    else throw Exception(E::FailedToOpenFile, "Unsupported format: ", filename);

    changed = true;
}

void GuiTexture::FlipImage(char* data, const uint32_t& lineLen, const uint32_t& lines)
{
    std::vector<char> temp(lineLen);

    for (int32_t i = 0; i < lines/2; i++)
    {
        memcpy(temp.data(), data + i * lineLen, lineLen);
        memcpy(data + i * lineLen, data + (lines - i - 1) * lineLen, lineLen);
        memcpy(data + (lines - i - 1) * lineLen, temp.data(), lineLen);
    }
}

void GuiTexture::Clear()
{
    bufferMutex.lock();
    glBindTexture(GL_TEXTURE_2D, texture);
    memset(rgbBuffer.data(), 0, rgbBuffer.size());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, resolution.x, resolution.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, rgbBuffer.data());
    bufferMutex.unlock();
}

void GuiTexture::Update()
{
    if (changed)
    {
        bufferMutex.lock();
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, resolution.x, resolution.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, rgbBuffer.data());
        changed = false;
        bufferMutex.unlock();
    }
}

