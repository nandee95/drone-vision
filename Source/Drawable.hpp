#pragma once

#include <glm/mat4x4.hpp>

struct DrawInfo
{
    glm::mat4 PV;
    float deltaTime;
    float elapsedTime;
    glm::vec3 cameraPos;
};

class Drawable
{
public:
    virtual void Draw(const DrawInfo& info) = 0;
};