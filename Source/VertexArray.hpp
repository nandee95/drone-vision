#pragma once
#include <glad/glad.h>
#include <vector>

template <bool D,bool E>
class VertexArrayPrototype
{
protected:
	GLuint VBO, VAO, EBO;

	static constexpr size_t glSizeof[] = {
		1, //GL_BYTE
		sizeof(GLubyte),
		sizeof(GLshort),
		sizeof(GLushort),
		sizeof(GLint),
		sizeof(GLuint),
		sizeof(GLfloat),
		2,3,4,
		sizeof(GLdouble)		
	};

public:
	struct Attrib
	{
		size_t count;
		GLenum type=GL_FLOAT;
	};


	void Create()
	{
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
	}
	~VertexArrayPrototype()
	{
		if (VAO != 0)
		{
			glDeleteVertexArrays(1, &VAO);
			glDeleteBuffers(1, &VBO);
			if (E) glDeleteBuffers(1, &EBO);
		}
	}

	void Bind()
	{
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
	}

	void FillVertices(const void* vertices,const size_t& length)
	{
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, length, vertices, D ? GL_STREAM_DRAW : GL_STATIC_DRAW);
	}

	template<typename T = GLfloat>
	void FillVertices(const std::vector<T> vertices)
	{
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(T), vertices.data(), D ? GL_STREAM_DRAW : GL_STATIC_DRAW);
	}

	void UpdateVertices(const size_t offset,const void* data, const size_t size)
	{
		static_assert(D, "UpdateBuffer method can only be used on a Dynamic VertexArray");
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferSubData(GL_ARRAY_BUFFER, offset, size, data);
	}

	void FillIndices(const GLuint* data, const size_t length)
	{
		static_assert(E, "FillIndices method can only be used on an ElementArray");
		glBindVertexArray(VAO);
		glGenBuffers(1, &EBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, length * sizeof(GLuint), data, D ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
	}

	void FillIndices(const std::vector<GLuint> indices)
	{
		static_assert(E, "FillIndices method can only be used on an ElementArray");
		glBindVertexArray(VAO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), D ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
	}

	void DrawElements(const GLenum mode, const size_t count) const
	{
		static_assert(E, "DrawElement method can only be used on an ElementArray");

		glBindVertexArray(VAO);
		glDrawElements(mode, count, GL_UNSIGNED_INT, 0);
	}

	void DrawArrays(const GLenum mode, const size_t from, const size_t to) const
	{
		static_assert(!E, "DrawArray method can only be used on a VertexArray");
		glBindVertexArray(VAO);
		glDrawArrays(mode, from, to);
	}

	//count, type
	void SetAttribPointers(const std::initializer_list<Attrib> attribs) const
	{
		glBindVertexArray(VAO);
		GLuint pos = 0, total = 0, off=0;
		for (const auto& a : attribs) total += a.count * glSizeof[a.type - GL_BYTE];

		for (const auto& a : attribs)
		{
			glVertexAttribPointer(pos, a.count, a.type, GL_FALSE, total, (void*)(off * glSizeof[a.type - GL_BYTE]));
			glEnableVertexAttribArray(pos++);
			off += a.count;
		}
	}

	const GLuint GetVAO() const
	{
		return VAO;
	}

	const GLuint GetVBO() const
	{
		return VBO;
	}

};

typedef VertexArrayPrototype<false,false> VertexArray;
typedef VertexArrayPrototype<true,false> DynamicVertexArray;
typedef VertexArrayPrototype<false,true> ElementArray;
typedef VertexArrayPrototype<true,true> DynamicElementArray;
