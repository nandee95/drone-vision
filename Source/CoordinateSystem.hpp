#pragma once

#include "Drawable.hpp"
#include "ObjMesh.hpp"

class CoordinateSystem : public Drawable
{
	ObjMesh mesh;
public:

	void Create();

	virtual void Draw(const DrawInfo& info);

};