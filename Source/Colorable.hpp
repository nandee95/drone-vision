#pragma once

#include <glm/vec4.hpp>

class Colorable
{
protected:
    glm::vec4 color{1,1,1,1};
public:
    const void SetColor(const glm::vec4& color)
    {
        this->color = color;
    }

    const glm::vec4 GetColor() const 
    {
        return color;
    }
};

