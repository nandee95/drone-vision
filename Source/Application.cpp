#include "Application.hpp"

Application::Application()
{
	if (glfwInit() != GLFW_TRUE)
	{
		std::cerr << "Failed to init glfw" << std::endl;
		exit(EXIT_FAILURE);
	}

	glfwWindowHint(GLFW_VERSION_MINOR, 4);
	glfwWindowHint(GLFW_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_SAMPLES, 8);

	window = glfwCreateWindow(1280, 720, "Drone vision", nullptr, nullptr);
	camera.SetAspectRatio(1280.f / 720.f);
	camera.SetPosition({ -10000, 3000, 10000 });
	camera.SetOrientation({ 0.707, 0, 0.707, 0 });

	if (!window)
	{
		std::cerr << "Failed to create window" << std::endl;
		exit(EXIT_FAILURE);
	}

	glfwSetWindowSizeLimits(window, 800, 600, GLFW_DONT_CARE, GLFW_DONT_CARE);
	glfwSetWindowUserPointer(window, this);

	moveCursor = glfwCreateStandardCursor(GLFW_CROSSHAIR_CURSOR);
	arrowCursor = glfwCreateStandardCursor(GLFW_ARROW_CURSOR);

	glfwMakeContextCurrent(window);

	screen = new nanogui::Screen();
	screen->initialize(window, false);

	cameraUniform.Create(&cameraData, sizeof(cameraData), GL_STREAM_DRAW);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_MULTISAMPLE);

	glClearColor(0, 0, 0, 1);

	glViewport(0, 0, 1280, 720);
}

int32_t Application::Run()
{
	camera.SetFOV(glm::half_pi<float>());
	projection = camera.CalcProjMatrix(10);

	Grid grid;

	std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
	std::chrono::high_resolution_clock::time_point lastFrame = start;

	DrawInfo info;
	cameraUniform.BindBufferBase(0);


	ObjMesh::Init();
	Person::Init();
	Box::Init();

	CoordinateSystem cs;
	cs.Create();

	// Create gui windows
	parserWindow.Create(screen);
	playbackWindow.Create(screen);
	segmentWindow.Create(screen, { 1280, 720 });
	windows.push_back(dynamic_cast<GuiWindow*>(&playbackWindow));
	windows.push_back(dynamic_cast<GuiWindow*>(&parserWindow));
	windows.push_back(dynamic_cast<GuiWindow*>(&segmentWindow));

	screen->setVisible(true);
	screen->performLayout();

	RegisterWindowCallbacks();
	RegisterGuiCallbacks();

	drone.Create();
	drone.SetColor({ 1, 1, 1, 1 });
	drone.SetPosition({ 5000, 5000, 5000 });

	while (!glfwWindowShouldClose(window))
	{
		// Update info
		const std::chrono::high_resolution_clock::time_point newFrame = std::chrono::high_resolution_clock::now();
		info.deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(newFrame - lastFrame).count() / 1e6;
		info.elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(newFrame - start).count() / 1e6;
		lastFrame = newFrame;

		// Controls
		if (cameraLocked && !dataset.frames.empty())
		{
			camera.SetPosition(dataset.frames[currentFrame].position * 1000.f);
			glm::vec3 euler = glm::radians(dataset.frames[currentFrame].orientation);
			euler.z -= glm::pi<float>();
			camera.SetOrientation(glm::vec3(0, 0, glm::pi<float>()));
			camera.Turn2(euler);
		}
		else
		{
			float boostSpeed = 1.f;

			if (glfwGetKey(window, GLFW_KEY_F))
				boostSpeed = 25.f;

			camera.Move({
				(glfwGetKey(window,GLFW_KEY_D) ? 1.f : (glfwGetKey(window,GLFW_KEY_A) ? -1.f : 0.f)) * cameraSpeed *
				boostSpeed * info.deltaTime,
				(glfwGetKey(window,GLFW_KEY_SPACE) ? 1.f : (glfwGetKey(window,GLFW_KEY_LEFT_SHIFT) ? -1.f : 0.f)) *
				cameraSpeed * boostSpeed * info.deltaTime,
				(glfwGetKey(window,GLFW_KEY_W) ? 1.f : (glfwGetKey(window,GLFW_KEY_S) ? -1.f : 0.f)) * cameraSpeed *
				boostSpeed * info.deltaTime,
				});
		}
		info.cameraPos = camera.GetPosition();

		cameraData.projView = projection * camera.CalcViewMatrix();
		cameraData.cameraPos = info.cameraPos;
		cameraUniform.Upload(&cameraData, 0, sizeof(cameraData));

		// Render frame
		cameraUniform.BindBufferBase(0);

		// Set OpenGL state (Nanogui messes with it)
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glEnable(GL_CULL_FACE);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthFunc(GL_LESS);
		glEnable(GL_STENCIL_TEST);
		glStencilOp(GL_ZERO, GL_ZERO, GL_REPLACE);
		glCullFace(GL_FRONT);
		glStencilMask(0xFF);
		glStencilFunc(GL_ALWAYS, 0, 0xFF);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

		// Draw entities
		grid.Draw(info);

		cs.Draw(info);

		objectMutex.lock();

		// Write outlined objects to the stencil buffer

		glStencilFunc(GL_ALWAYS, 1, 0xFF);
		glStencilOp(GL_ZERO, GL_ZERO, GL_REPLACE);
		for (auto& o : objects) { for (auto& e : o.second.objects) { e->Draw(info); } }

		// Substract not utlined objects from the stencil buffer
		for (auto& o : objects) { for (auto& e : o.second.objects) { e->Draw(info); } }
		objectMutex.unlock();

		if (!cameraLocked)
			drone.Draw(info);

		// Update gui
		if (playing) { playbackWindow.SetFrame(currentFrame, dataset.frames.size(), dataset.cam.frameRate); }
		playbackWindow.rgbTexture.Update();
		playbackWindow.segTexture.Update();

		// Draw gui
		glDisable(GL_DEPTH_TEST);
		guiMutex.lock();
		//screen->drawContents();
		screen->drawWidgets();
		guiMutex.unlock();

		// Handle window events & swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	return EXIT_SUCCESS;
}

Application::~Application()
{
	parsingInProgress = false;
	playing = false;
	if (parserThread.joinable()) { parserThread.join(); }
	if (playbackThread.joinable()) { playbackThread.join(); }
}

void Application::SetGuiState(const GuiState& state)
{
	guiMutex.lock();
	for (auto& w : windows) { w->SetState(state); }
	guiMutex.unlock();
}

glm::vec3 Application::CalcWorldVector(const glm::vec2& screenPos, const glm::vec2& screenResolution,
	const glm::quat& orientation) const
{
	glm::mat4 proj = glm::perspective(glm::radians(60.f), screenResolution.x / screenResolution.y, 1.f, 999999.f);
	glm::mat4 view = glm::lookAt(glm::vec3(0, 0, 0), orientation * glm::vec3(0, 0, 1),
		orientation * glm::vec3(0, 1, 0));

	glm::vec2 screen = (screenPos / screenResolution * 2.f - 1.f);
	return glm::normalize(glm::vec3(
		glm::inverse(proj * view) * glm::vec4(screen, 0.0f, 1.0f)
	));
}

void Application::PlaybackThread(Application* that, bool singleFrame)
{
	while (that->playing)
	{
		auto start = std::chrono::high_resolution_clock::now();
		that->drone.SetPosition(that->dataset.frames[that->currentFrame].position * 1000.f);
		that->drone.SetOrientation(glm::radians(that->dataset.frames[that->currentFrame].orientation));

		for (auto& o : that->objects)
		{
			for (auto& e : o.second.objects)
			{
				e->SetNextOutLine(false); // Only written from this thread no need to lock the mutex
			}
		}

		try
		{
			that->playbackWindow.rgbTexture.SetImage(that->dataset.frames[that->currentFrame].rgbImage.c_str());
			that->playbackWindow.segTexture.SetImage(that->dataset.frames[that->currentFrame].segImage.c_str());
			auto& res = that->playbackWindow.segTexture.resolution;
			auto result = that->detector.Detect(that->playbackWindow.segTexture.rgbBuffer.data(), res);;

			for (auto& r : result)
			{
				r.midPoint.x = res.x - r.midPoint.x - 1;
				r.midPoint.y = res.y - r.midPoint.y - 1;
			}

			std::vector<DroneRay> newRays;

			for (auto& blob : result)
			{
				if (blob.touchesEdge)
					continue;

				glm::vec3 unitVec = that->CalcWorldVector(blob.midPoint, that->playbackWindow.segTexture.resolution,
					that->drone.GetOrientation());

				float dist;

				float& height = that->objects.at(blob.group->name).blobData.avgHeight;

				if (glm::intersectRayPlane(that->drone.GetPosition(), unitVec, glm::vec3(0, height, 0),
					glm::vec3(0, 1, 0), dist))
				{
					if (dist < that->objects.at(blob.group->name).blobData.minDistance || dist > that->objects.
						at(blob.group->name).blobData.maxDistance)
						continue;
					glm::vec3 projectedPos = that->drone.GetPosition() + (unitVec * dist) + glm::vec3(0, -height, 0);

					newRays.push_back({
						that->drone.GetPosition(), projectedPos + glm::vec3(0, height, 0), blob.group->color
						});
					bool found = false;

					for (auto it = that->objects.at(blob.group->name).objects.begin(); it != that->objects.
						at(blob.group->name).objects.end(); it++)
					{
						auto ptr = *it;
						if (glm::distance(ptr->GetPosition(), projectedPos) > 3000)
							continue;

						that->objectMutex.lock();
						ptr->SetPosition(projectedPos);
						ptr->SetNextOutLine(true);
						that->objectMutex.unlock();

						for (auto it2 = that->objects.at(blob.group->name).objects.begin(); it2 != that->objects.
							at(blob.group->name).objects.end();)
						{
							auto ptr2 = *it2;
							if (it == it2 || glm::distance(ptr2->GetPosition(), projectedPos) > 300)
							{
								it2++;
								continue;
							}

							that->objectMutex.lock();
							it2 = that->objects.at(blob.group->name).objects.erase(it2);
							that->objectMutex.unlock();
						}
						found = true;
						break;
					}
					if (!found)
					{
						//blob.group->name
						auto& renderer = that->dataset.segments.at(blob.group->name).renderer;

						if (renderer.type == Dataset::SegmentType::Person)
						{
							that->objectMutex.lock();
							auto person = std::make_shared<Person>();
							that->objects.at(blob.group->name).objects.push_back(person);
							person->SetPosition(projectedPos);
							person->SetColor(glm::vec4(blob.group->color, 1));
							person->SetScale({ 800.f, renderer.height, 800.f });
							person->SetOutLine(true);
							person->SetNextOutLine(true);
							that->objectMutex.unlock();
						}
						else if (renderer.type == Dataset::SegmentType::Box)
						{
							that->objectMutex.lock();
							auto box = std::make_shared<Box>();
							that->objects.at(blob.group->name).objects.push_back(box);
							box->SetPosition(projectedPos);
							box->SetColor(glm::vec4(blob.group->color, 1));
							box->SetScale(renderer.size);
							box->SetOutLine(true);
							box->SetNextOutLine(true);
							that->objectMutex.unlock();
						}
					}
				}
			}

			that->drone.rayMutex.lock();
			that->drone.rays.swap(newRays);
			that->drone.rayMutex.unlock();

			that->guiMutex.lock();
			for (auto& o : that->objects) { that->segmentWindow.SetCount(o.first, o.second.objects.size()); }
			that->playbackWindow.SetCamera(that->drone.GetPosition(), that->drone.GetOrientation());
			that->guiMutex.unlock();

			that->objectMutex.lock();
			for (auto& o : that->objects) { for (auto& e : o.second.objects) { e->UpdateOutline(); } }
			that->objectMutex.unlock();
		}
		catch (Exception& e) { std::cerr << e.GetMessage() << std::endl; }

		that->currentFrame = (that->currentFrame + 1) % that->dataset.frames.size();

		if (singleFrame)
		{
			that->SetGuiState(GuiState::Paused);
			that->playing = false;
			return;
		}

		const auto stop = std::chrono::high_resolution_clock::now();

		const float elapsed = std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count() / 1e6;

		const float target = that->playbackWindow.rateSlider->value() * 450.f + 50.f;

		if (target != 500.f)
		{
			float expected = 1.f / (target / 100.f) * (1.f / that->dataset.cam.frameRate);
			if (elapsed < expected)
				std::this_thread::sleep_for(
					std::chrono::microseconds(static_cast<uint32_t>((expected - elapsed) * 1e6)));
		}

		that->playbackWindow.Tick(that->dataset.cam.frameRate);
	}
}

void Application::ParserThread(Application* that, const std::string& path)
{
	that->dataset.Reset();
	that->SetGuiState(GuiState::Parsing);
	try
	{
		that->dataset.Load(path.c_str(), [&](std::string what, int progress, int target)-> bool {
			that->guiMutex.lock();
			that->parserWindow.label->setCaption(
				(std::string)"Parsing " + what + " " + std::to_string(progress) + "/" + std::to_string(target) +
				" ...");
			that->parserWindow.slider->setValue(static_cast<float>(progress) / target);
			that->guiMutex.unlock();

			return that->parsingInProgress;
			});
	}
	catch (Exception& e)
	{
		that->parsingInProgress = false;

		std::cerr << "Failed to parse file: " << e.GetMessage() << std::endl;
		return;
	}
	that->SetGuiState(GuiState::Stopped);
	that->guiMutex.lock();
	that->playbackWindow.SetFrame(0, that->dataset.frames.size(), that->dataset.cam.frameRate);

	that->objectMutex.lock();
	for (auto o : that->objects) { o.second.objects.clear(); }
	that->objects.clear();
	that->segmentWindow.Clear();
	that->detector.ClearSegmentGroups();
	for (const auto& order : { Dataset::SegmentType::Person, Dataset::SegmentType::Box, Dataset::SegmentType::None })
	{
		for (const auto& s : that->dataset.segments)
		{
			if (s.second.renderer.type != order)
				continue;
			that->segmentWindow.AddRow(s, 0);

			if (Dataset::SegmentType::None == order)
				continue;

			ObjectData obj;
			obj.blobData = s.second.details;
			that->objects.insert(std::make_pair(s.first, obj));
			SegmentGroup sg;
			sg.color.r = static_cast<uint8_t>(std::floor(s.second.color.r * 255.f));
			sg.color.g = static_cast<uint8_t>(std::floor(s.second.color.g * 255.f));
			sg.color.b = static_cast<uint8_t>(std::floor(s.second.color.b * 255.f));
			sg.name = s.first;
			that->detector.AddSegmentGroup(sg);
		}
	}
	that->screen->performLayout();

	that->drone.rayMutex.lock();
	that->drone.rays.clear();
	that->drone.rayMutex.unlock();

	that->objectMutex.unlock();

	that->guiMutex.unlock();
	that->drone.SetFov(glm::radians(that->dataset.cam.fov),
		glm::radians(that->dataset.cam.fov) * that->dataset.cam.resolution.y / that->dataset.cam.
		resolution.x);
}

void Application::RegisterGuiCallbacks()
{
	playbackWindow.openButton->setCallback([&]() {
		playbackWindow.openButton->setPushed(false);

		auto path = nanogui::file_dialog({ {"json", "JSON configuration file"} }, false);

		if (path.empty())
			return;
		parserWindow.SetFile(path);

		parsingInProgress = true;
		if (parserThread.joinable())
			parserThread.join();
		parserThread = std::thread(ParserThread, this, path);

		playbackWindow.rgbTexture.Clear();
		playbackWindow.segTexture.Clear();

		currentFrame = 0;

		});

	playbackWindow.playPauseButton->setCallback([&]() {
		playbackWindow.playPauseButton->setPushed(false);

		playing = !playing;

		playbackWindow.playPauseButton->setIcon(!playing ? ENTYPO_ICON_CONTROLLER_PLAY : ENTYPO_ICON_CONTROLLER_PAUS);

		if (playing)
		{
			SetGuiState(GuiState::Playing);
			playbackThread = std::thread(PlaybackThread, this, false);
		}
		else
		{
			SetGuiState(GuiState::Paused);
			if (playbackThread.joinable())
				playbackThread.join();
			playbackWindow.SetFrame(currentFrame, dataset.frames.size(), dataset.cam.frameRate, "paused");
		}
		});

	playbackWindow.stopButton->setCallback([&]() {
		playbackWindow.stopButton->setPushed(false);

		playing = false;
		currentFrame = 0;

		SetGuiState(GuiState::Stopped);
		if (playbackThread.joinable())
			playbackThread.join();
		playbackWindow.SetFrame(0, dataset.frames.size(), dataset.cam.frameRate, "stopped");

		playbackWindow.rgbTexture.Clear();
		playbackWindow.segTexture.Clear();
		});

	playbackWindow.nextButton->setCallback([&]() {
		playbackWindow.nextButton->setPushed(false);

		playing = true;
		playbackThread = std::thread(PlaybackThread, this, true);
		if (playbackThread.joinable())
			playbackThread.join();
		playing = false;
		playbackWindow.SetFrame(currentFrame, dataset.frames.size(), dataset.cam.frameRate, "paused");
		});

	parserWindow.cancelButton->setCallback([&]() {
		if (parserThread.joinable()) { parserThread.join(); }
		dataset.Reset();
		});

	playbackWindow.jumpToCam->setCallback([&]() {
		playbackWindow.jumpToCam->setPushed(false);
		if (dataset.frames.empty())
			return;

		camera.SetPosition(dataset.frames[currentFrame].position * 1000.f);
		glm::vec3 euler = glm::radians(dataset.frames[currentFrame].orientation);
		euler.z -= glm::pi<float>();
		camera.SetOrientation(glm::vec3(0, 0, glm::pi<float>()));
		camera.Turn2(euler);
		});

	playbackWindow.lockCam->setChangeCallback([&](bool state) { cameraLocked = state; });
	playbackWindow.slider->setFinalCallback([&](float value) {
		currentFrame = static_cast<int>(value * (dataset.frames.size() - 1));
		playing = true;
		playbackThread = std::thread(PlaybackThread, this, true);
		if (playbackThread.joinable())
			playbackThread.join();
		playing = false;
		playbackWindow.SetFrame(currentFrame, dataset.frames.size(), dataset.cam.frameRate, "paused");
		});
	playbackWindow.slider->setCallback([&](float value) {
		currentFrame = static_cast<int>(value * (dataset.frames.size() - 1));
		playbackWindow.SetFrame(currentFrame, dataset.frames.size(), dataset.cam.frameRate, "seeking");
		});

	playbackWindow.rateSlider->setCallback([&](float value) {
		if (value != 1.f)
		{
			const uint32_t slider = value * 450.f + 50.f;
			playbackWindow.currentRateLabel->setCaption(
				"Playback rate: " + std::to_string(
					static_cast<uint32_t>(static_cast<float>(playbackWindow.rate) * 100.f / dataset.cam.frameRate)) +
				"% / " + std::to_string(slider) + " % ");
		}
		else
		{
			playbackWindow.currentRateLabel->setCaption(
				"Playback rate: " + std::to_string(
					static_cast<uint32_t>(static_cast<float>(playbackWindow.rate) * 100.f / dataset.cam.frameRate)) +
				"% (unlimited)");
		}
		});
}

void Application::RegisterWindowCallbacks()
{
	glfwSetCursorPosCallback(window, [](GLFWwindow* window, double x, double y) {
		Application* that = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));
		if (that->mouseLocked)
		{
			const glm::vec2 delta = (glm::vec2{ x, y } - that->oldCursorPos) * cameraSensitivity;
			that->camera.Turn(glm::vec3(-delta.y, -delta.x, 0));
		}
		else
			that->screen->cursorPosCallbackEvent(x, y);

		that->oldCursorPos.x = x;
		that->oldCursorPos.y = y;
		});

	glfwSetKeyCallback(window, [](GLFWwindow* window, int key, int scancode, int action, int mods) {
		Application* that = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));

		if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		{
			glfwSetWindowShouldClose(window, GLFW_TRUE);
			return;
		}

		if (key == GLFW_KEY_F11 && action == GLFW_PRESS)
		{
			auto* monitor = glfwGetWindowMonitor(window);
			if (!monitor) // from windowed to fullscreen
			{
				glfwGetWindowSize(window, &that->oldWindowSize.x, &that->oldWindowSize.y);
				glfwGetWindowPos(window, &that->oldWindowPos.x, &that->oldWindowPos.y);

				monitor = glfwGetPrimaryMonitor();
				auto* videoMode = glfwGetVideoMode(monitor);

				const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
				glfwSetWindowMonitor(window, monitor, 0, 0, videoMode->width, videoMode->height, GLFW_DONT_CARE);
				glViewport(0, 0, videoMode->width, videoMode->height);
				that->camera.SetAspectRatio(static_cast<float>(videoMode->width) / videoMode->height);
			}
			else // from fullscreen to windowed
			{
				glfwSetWindowMonitor(window, nullptr, that->oldWindowPos.x, that->oldWindowPos.y, that->oldWindowSize.x,
					that->oldWindowSize.y, GLFW_DONT_CARE);
				glViewport(0, 0, that->oldWindowSize.x, that->oldWindowSize.y);
				that->camera.SetAspectRatio(static_cast<float>(that->oldWindowSize.y) / that->oldWindowSize.x);
			}
			return;
		}
		that->screen->keyCallbackEvent(key, scancode, action, mods);
		});

	glfwSetMouseButtonCallback(window,
		[](GLFWwindow* window, int button, int action, int modifiers) {
			auto* that = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));

			if (that->screen->mouseButtonCallbackEvent(button, action, modifiers)) { return; }

			if (button == GLFW_MOUSE_BUTTON_LEFT && !that->cameraLocked)
			{
				that->mouseLocked = action == GLFW_PRESS;
				glfwSetCursor(window, that->mouseLocked ? that->moveCursor : that->arrowCursor);
				if (that->mouseLocked)
				{
					glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
				}
				else { glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL); }
			}
		}
	);

	glfwSetCharCallback(window,
		[](GLFWwindow* window, unsigned int codepoint) {
			reinterpret_cast<Application*>(glfwGetWindowUserPointer(window))->screen->charCallbackEvent(
				codepoint);
		}
	);

	glfwSetDropCallback(window,
		[](GLFWwindow* window, int count, const char** filenames) {
			reinterpret_cast<Application*>(glfwGetWindowUserPointer(window))->screen->dropCallbackEvent(
				count, filenames);
		}
	);

	glfwSetScrollCallback(window,
		[](GLFWwindow* window, double x, double y) {
			reinterpret_cast<Application*>(glfwGetWindowUserPointer(window))->screen->
				scrollCallbackEvent(x, y);
		}
	);

	glfwSetFramebufferSizeCallback(window,
		[](GLFWwindow* window, int width, int height) {
			Application* that = reinterpret_cast<Application*>(glfwGetWindowUserPointer(
				window));

			float aspect = static_cast<float>(width) / height;
			if (aspect < 0.1 || std::isnan(aspect))
				return;

			that->screen->resizeCallbackEvent(width, height);
			for (auto& w : that->windows) { w->Resize({ width, height }); }

			glViewport(0, 0, width, height);

			that->camera.SetAspectRatio(aspect);
			that->projection = that->camera.CalcProjMatrix(10);
		}
	);
}
