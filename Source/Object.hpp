#pragma once

#include "Drawable.hpp"
#include "Transformable.hpp"
#include "Colorable.hpp"

class Object : public Drawable, public Transformable, public Colorable
{
protected:
	bool outline = false;
	bool nextOutline = false;
public:
	void SetOutLine(const bool& outline)
	{
		this->outline = outline;
	}

	bool GetOutline() const
	{
		return outline;
	}
	
	void SetNextOutLine(const bool& nextOutline)
	{
		this->nextOutline = nextOutline;
	}

	bool GetNextOutline() const
	{
		return nextOutline;
	}

	void UpdateOutline()
	{
		outline = nextOutline;
	}
};