#pragma once

#include <chrono>

#include <nanogui/nanogui.h>
#include <glm/gtx/matrix_interpolation.hpp>

#include "GuiTexture.hpp"

#include "GuiWindow.hpp"

namespace nanogui
{
    class LabelButtonLayout : public BoxLayout {
        int32_t fixFirst = 7;
    public:
    	
        LabelButtonLayout(Orientation orientation, Alignment alignment = Alignment::Middle,
            int margin = 0, int spacing = 0,int32_t fixFirst = 7) : BoxLayout(orientation, alignment,margin,spacing) , fixFirst(fixFirst) {
        }

        virtual void performLayout(NVGcontext* ctx, Widget* widget) const override
        {
            Vector2i fs_w = widget->fixedSize();
            Vector2i containerSize(
                fs_w[0] ? fs_w[0] : widget->width(),
                fs_w[1] ? fs_w[1] : widget->height()
            );

            int axis1 = (int)mOrientation, axis2 = ((int)mOrientation + 1) % 2;
            int position = mMargin;
            int yOffset = fixFirst;

            bool first = true;
            for (auto w : widget->children()) {
                if (!w->visible())
                    continue;

                if (!first) position -= mSpacing;
            	
                Vector2i ps = w->preferredSize(ctx), fs = w->fixedSize();
                Vector2i targetSize(
                    fs[0] ? fs[0] : ps[0],
                    fs[1] ? fs[1] : ps[1]
                );
                Vector2i pos(0, yOffset);

                if (!first)
                    position -= targetSize[axis1];

                pos[axis1] = position;

                w->setPosition(pos);
                w->setSize(targetSize);
                w->performLayout(ctx);


                if (first)
                {
                    first = false;
					position = containerSize.x() - mMargin + mSpacing;
                    yOffset = 0;
				}
            }
        }
    };
}

class PlaybackWindow : public GuiWindow
{
public:
    int32_t fps = 0, rate=0;
    std::chrono::high_resolution_clock::time_point lastFrame;
	
    nanogui::Window* window;
    nanogui::Label* currentFrameLabel,* currentTimeLabel,* currentRateLabel, * camPosLabelX, * camPosLabelY, * camPosLabelZ,* camOriLabelX,* camOriLabelY,* camOriLabelZ;
    nanogui::ImageView* rgbImageView, * segImageView;
    nanogui::ToolButton* stopButton, * playPauseButton, * openButton, * nextButton;
    nanogui::Slider* slider;
    nanogui::ToolButton* jumpToCam,*lockCam;
    GuiTexture rgbTexture, segTexture;
    nanogui::ToolButton* inputResetBtn, * segmentResetBtn;
    nanogui::Slider* rateSlider;

    void Create(nanogui::Screen* screen)
    {
        window = new nanogui::Window(screen, "Playback");
        window->setFixedWidth(200);
        window->setPosition(nanogui::Vector2i(15, 15));
        window->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Vertical,nanogui::Alignment::Fill,6,6));

        nanogui::Widget* controlWidget = new nanogui::Widget(window);
        controlWidget->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Vertical, nanogui::Alignment::Middle, 0, 0));

        nanogui::Widget* controlInnerWidget = new nanogui::Widget(controlWidget);
        controlInnerWidget->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Horizontal, nanogui::Alignment::Minimum, 0, 6));

        stopButton = new nanogui::ToolButton(controlInnerWidget, ENTYPO_ICON_CONTROLLER_STOP);
        stopButton->setTooltip("Stops the playback, jumps to frame 0");
        stopButton->setEnabled(false);

        playPauseButton = new nanogui::ToolButton(controlInnerWidget, ENTYPO_ICON_CONTROLLER_PLAY);
        playPauseButton->setTooltip("Play/pause segmentation");
        playPauseButton->setEnabled(false);

        nextButton = new nanogui::ToolButton(controlInnerWidget, ENTYPO_ICON_CONTROLLER_NEXT);
        nextButton->setTooltip("Jumps to the next frame (when paused)");
        nextButton->setEnabled(false);

        openButton = new nanogui::ToolButton(controlInnerWidget, ENTYPO_ICON_FOLDER_IMAGES);
        openButton->setTooltip("Open dataset file");
        openButton->setCursor(nanogui::Cursor::Hand);

        slider = new nanogui::Slider(window);
        slider->setValue(0.0);
        slider->setEnabled(false);


        auto timeWidget = new nanogui::Widget(window);
        timeWidget->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Vertical, nanogui::Alignment::Middle, 0, 0));
        currentTimeLabel = new nanogui::Label(timeWidget, "00:00:00 / 00:00:00");


        currentFrameLabel = new nanogui::Label(window, "Frame: 0/0");

        currentRateLabel = new nanogui::Label(window, "Playback rate: 0% / 100%                     ");

        rateSlider = new nanogui::Slider(window);
        rateSlider->setValue(1.f/9.f); // 100%
    	
        rgbTexture.Create();
        auto inputWidget = new nanogui::Widget(window);
        auto inputLayout = new nanogui::LabelButtonLayout(nanogui::Orientation::Horizontal,nanogui::Alignment::Fill,0,0);
        inputWidget->setLayout(inputLayout);
        new nanogui::Label(inputWidget, "Input preview");

        inputResetBtn = new nanogui::ToolButton(inputWidget, ENTYPO_ICON_CYCLE);
        inputResetBtn->setCursor(nanogui::Cursor::Hand);
        inputResetBtn->setCallback([&]() {
            inputResetBtn->setPushed(false);
            rgbImageView->setScale(188.f / 1280.f);
            rgbImageView->setOffset({ 0,0 });
            });
        rgbImageView = new nanogui::ImageView(window, rgbTexture.texture);
        rgbImageView->setScale(188.f / 1280.f);
        rgbImageView->setFixedSize({ 188.f,9.f * 188.f / 16.f });


        segTexture.Create();
        auto segWidget = new nanogui::Widget(window);
        auto segLayout = new nanogui::LabelButtonLayout(nanogui::Orientation::Horizontal, nanogui::Alignment::Fill, 0, 0);
       segWidget->setLayout(segLayout);
        new nanogui::Label(segWidget, "Segmented preview");

        segmentResetBtn = new nanogui::ToolButton(segWidget, ENTYPO_ICON_CYCLE);
        segmentResetBtn->setCursor(nanogui::Cursor::Hand);
        segmentResetBtn->setCallback([&]() {
            segmentResetBtn->setPushed(false);
            segImageView->setScale(188.f / 1280.f);
            segImageView->setOffset({ 0,0 });
            });

        segImageView = new nanogui::ImageView(window, segTexture.texture);
        segImageView->setScale(188.f / 1280.f);
        segImageView->setFixedSize({ 188.f,9.f * 188.f / 16.f });


        auto camWidget = new nanogui::Widget(window);
        camWidget->setLayout(new nanogui::LabelButtonLayout(nanogui::Orientation::Horizontal, nanogui::Alignment::Minimum, 0, 6));

        new nanogui::Label(camWidget, "Drone camera");
    	
        lockCam = new nanogui::ToolButton(camWidget, ENTYPO_ICON_EYE);
        lockCam->setTooltip("Locks the camera to the drone");
        lockCam->setCursor(nanogui::Cursor::Hand);
    	
        jumpToCam = new nanogui::ToolButton(camWidget, ENTYPO_ICON_LOGIN);
        jumpToCam->setTooltip("Jumps to the drone");
        jumpToCam->setCursor(nanogui::Cursor::Hand);
        auto widget = new nanogui::Widget(window);
        widget->setLayout(new nanogui::GridLayout(nanogui::Orientation::Horizontal, 2, nanogui::Alignment::Fill, 0, 0));

        new nanogui::Label(widget, "Position:");
        new nanogui::Label(widget, "Orientation:");

        camPosLabelX= new nanogui::Label(widget, "X:             ");
        camOriLabelX = new nanogui::Label(widget, "X:             ");
        camPosLabelY = new nanogui::Label(widget, "Y:             ");
        camOriLabelY = new nanogui::Label(widget, "Y:             ");
        camPosLabelZ = new nanogui::Label(widget, "Z:             ");
        camOriLabelZ = new nanogui::Label(widget, "Z:             ");

    }

    void SetCamera(const glm::vec3& position, const glm::quat& orientation)
    {
        glm::vec3 euler={};
        glm::vec3 unitVec = glm::normalize(glm::vec3(0, 0, 1) * orientation);
        euler.y = glm::degrees(atan2(unitVec.z, unitVec.x));
        euler.x = glm::degrees(atan2(unitVec.y, sqrt(unitVec.x * unitVec.x + unitVec.z * unitVec.z)));
    	
        camPosLabelX->setCaption("X: " + std::to_string(static_cast<int>(position.x)));
        camPosLabelY->setCaption("Y: " + std::to_string(static_cast<int>(position.y)));
        camPosLabelZ->setCaption("Z: " + std::to_string(static_cast<int>(position.z)));
        camOriLabelX->setCaption("X: " + std::to_string(static_cast<int>(euler.x)) );
        camOriLabelY->setCaption("Y: " + std::to_string(static_cast<int>(euler.y)) );
        camOriLabelZ->setCaption("Z: " + std::to_string(static_cast<int>(euler.z)) );
    }

    void SetRgbImage(const char* filename)
    {
        rgbTexture.SetImage(filename);

        rgbImageView->setScale(200.f / rgbTexture.resolution.x);
        rgbImageView->setFixedSize({ 200,200.f * rgbTexture.resolution.x / rgbTexture.resolution.y });
    }

    void SetSegImage(const char* filename)
    {
        segTexture.SetImage(filename);

        segImageView->setScale(200.f / segTexture.resolution.x);
        segImageView->setFixedSize({ 200,200.f * segTexture.resolution.x / segTexture.resolution.y });
    }

    static std::string TimeToString(uint32_t t)
    {
        const auto s = t%60;
        t -= s;
        t /= 60;
        const auto m = t%60;
        t -= m;
        t /= 60;
    	
        std::stringstream ss;
        if (t < 10) ss << 0;
        ss << static_cast<uint32_t>(t) << ":";

        if (m < 10) ss << 0;
        ss << m <<":";
        if (s < 10) ss << 0;
        ss << s;
    	
        return ss.str();
    }
	
    void SetFrame(const uint32_t& frame, const uint32_t& maxFrames,const uint32_t& frameRate, const std::string& note = "")
    {
        if (!note.empty())
            currentFrameLabel->setCaption("Frame: " + std::to_string(frame) + "/" + std::to_string(maxFrames) + " (" + note + ")");
        else
            currentFrameLabel->setCaption("Frame: " + std::to_string(frame) + "/" + std::to_string(maxFrames));

        currentTimeLabel->setCaption(TimeToString(frame/ frameRate)+ " / "+ TimeToString(maxFrames / frameRate));
    	
        slider->setValue(static_cast<float>(frame) / maxFrames);

    }

	void Tick(const uint32_t& frameRate)
    {
        auto now = std::chrono::high_resolution_clock::now();

        if (std::chrono::duration_cast<std::chrono::milliseconds>(now - lastFrame).count() > 1000)
        {
            rate = fps;
            if (rateSlider->value() != 1.f)
            {
                const uint32_t slider = rateSlider->value() * 450.f + 50.f;
                currentRateLabel->setCaption("Playback rate: " + std::to_string(static_cast<uint32_t>(static_cast<float>(rate) * 100.f / frameRate)) + "% / " + std::to_string(slider) + " % ");
            }
            else
            {
                currentRateLabel->setCaption("Playback rate: " + std::to_string(static_cast<uint32_t>(static_cast<float>(rate) * 100.f / frameRate)) + "% (unlimited)");
            }

            fps = 0;
            lastFrame = now;
        }
        else fps++;
    }

    virtual void SetState(const GuiState& state) override
    {
        switch (state)
        {
        case GuiState::Parsing:
        {
            stopButton->setEnabled(false);
            playPauseButton->setEnabled(false);
            playPauseButton->setIcon(ENTYPO_ICON_CONTROLLER_PAUS);
            nextButton->setEnabled(false);
            openButton->setEnabled(false);
            slider->setEnabled(false);
            break;
        }
        case GuiState::Paused:
        {
            stopButton->setEnabled(true);
            playPauseButton->setEnabled(true);
            playPauseButton->setIcon(ENTYPO_ICON_CONTROLLER_PLAY);
            nextButton->setEnabled(true);
            openButton->setEnabled(true);
            slider->setEnabled(true);
            break;
        }
        case GuiState::Stopped:
        {
            stopButton->setEnabled(true);
            playPauseButton->setEnabled(true);
            playPauseButton->setIcon(ENTYPO_ICON_CONTROLLER_PLAY);
            nextButton->setEnabled(true);
            openButton->setEnabled(true);
            slider->setEnabled(true);
            break;
        }
        case GuiState::Playing:
        {
            stopButton->setEnabled(true);
            playPauseButton->setEnabled(true);
            playPauseButton->setIcon(ENTYPO_ICON_CONTROLLER_PAUS);
            nextButton->setEnabled(false);
            openButton->setEnabled(false);
            slider->setEnabled(false);
            break;
        }
        }

        playPauseButton->setCursor(playPauseButton->enabled() ? nanogui::Cursor::Hand : nanogui::Cursor::Arrow);
        nextButton->setCursor(nextButton->enabled() ? nanogui::Cursor::Hand : nanogui::Cursor::Arrow);
        openButton->setCursor(openButton->enabled() ? nanogui::Cursor::Hand : nanogui::Cursor::Arrow);
        stopButton->setCursor(stopButton->enabled() ? nanogui::Cursor::Hand : nanogui::Cursor::Arrow);
    }

    virtual void Resize(const glm::vec2& resolution) override
    {
        window->setPosition(nanogui::Vector2i(15, 15));
    }
};