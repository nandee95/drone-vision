#pragma once

#include "Drawable.hpp"
#include "Transformable.hpp"
#include "Colorable.hpp"
#include "ObjMesh.hpp"

#include <mutex>

struct DroneRay
{
	glm::vec3 from;
	glm::vec3 fromColor;
	glm::vec3 to;
	glm::vec3 toColor;

	DroneRay(const glm::vec3& from, const glm::vec3& to, const glm::vec3& color)
		:from(from),to(to),fromColor(color),toColor(color)
	{

	}
};

class Drone : public Drawable, public Transformable, public Colorable
{
	ObjMesh mesh;
	ObjMesh frustum;
	DynamicVertexArray rayArray;
	Shader rayShader;
public:
	std::vector< DroneRay> rays;
	std::mutex rayMutex;

	void Create();
	void SetFov(const float& hfov, const float& vfov);

	virtual void Draw(const DrawInfo& info);

};