#pragma once

#include "GuiWindow.hpp"
#include <nanogui/nanogui.h>
#include <map>



class SegmentWindow : public GuiWindow
{
public:
    nanogui::Window* window;

    struct Row
    {
        nanogui::ToolButton* type;
        nanogui::Label* name;
        nanogui::Label* count;
    };

    std::map<std::string,Row> rows;
	void Create(nanogui::Screen* screen,const glm::vec2& resolution)
	{
        window = new nanogui::Window(screen, "Segments");
        window->setPosition(nanogui::Vector2i(resolution.x -200 -15, 15));
        window->setFixedWidth(200);
        window->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Vertical, nanogui::Alignment::Middle, 6, 6));

       auto label = new nanogui::Label(window, "No data");
	}

    void AddRow(const std::pair<std::string, Dataset::SegmentData>& data,const uint32_t& count)
    {
        nanogui::ToolButton* typeIcon;
        switch (data.second.renderer.type)
        {
        case Dataset::SegmentType::Box: typeIcon = new nanogui::ToolButton(window, ENTYPO_ICON_SQUARED_MINUS); break;
        case Dataset::SegmentType::Person: typeIcon = new nanogui::ToolButton(window, ENTYPO_ICON_MAN); break;
        default: typeIcon = new nanogui::ToolButton(window, ENTYPO_ICON_CROSS); break;
        }

        typeIcon->setCallback([typeIcon] {
            typeIcon->setEnabled(true);
            });

        typeIcon->setBackgroundColor(nanogui::Color{ data.second.color.r, data.second.color.g, data.second.color.b, 1.f });
        if(data.second.color.r * 0.5 + data.second.color.g * 0.5 + data.second.color.b * 0.2 >= 0.5)
            typeIcon->setTextColor(nanogui::Color{ 0.f, 0.f, 0.f , 1.f });
        else
            typeIcon->setTextColor(nanogui::Color{ 1.f, 1.f, 1.f , 1.f });
        auto widget = new nanogui::Widget(window);
        widget->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Vertical, nanogui::Alignment::Middle, 0, 0));
        auto nameLabel = new nanogui::Label(widget, data.first);

        auto widget2 = new nanogui::Widget(window);
        widget2->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Vertical, nanogui::Alignment::Middle, 0, 0));
        auto countLabel = new nanogui::Label(widget2, data.second.renderer.type != Dataset::SegmentType::None ? std::to_string(count)+"   " : "");
        rows.insert(std::make_pair(data.first, Row{ typeIcon,nameLabel,countLabel }));
    }

    void SetCount(const std::string& name,const uint32_t& count)
    {
        rows[name].count->setCaption(std::to_string(count));
    }


    void Clear()
    {
        while(window->childCount())
            window->removeChild(0);

        rows.clear();

        nanogui::GridLayout* layout = new nanogui::GridLayout(nanogui::Orientation::Horizontal, 3, nanogui::Alignment::Middle, 6, 6);
        layout->setColAlignment(
            { nanogui::Alignment::Minimum,nanogui::Alignment::Minimum, nanogui::Alignment::Fill , nanogui::Alignment::Minimum });
        layout->setSpacing(0, 10);
        window->setLayout(layout);

        new nanogui::Label(window, "Type");

        auto widget = new nanogui::Widget(window);
        widget->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Vertical, nanogui::Alignment::Middle, 0, 0));
        new nanogui::Label(widget, "Name");
        auto widget2 = new nanogui::Widget(window);
        widget2->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Vertical, nanogui::Alignment::Middle, 0, 0));
        new nanogui::Label(widget2, "Count");
    }

    virtual void SetState(const GuiState& state) override
    {}


    virtual void Resize(const glm::vec2& resolution) override
    {
        window->setPosition(nanogui::Vector2i(resolution.x - 200 - 15, 15));
    }
};