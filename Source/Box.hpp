#pragma once

#include "ObjMesh.hpp"
#include "Object.hpp"

class Box : public Object
{
	static ObjMesh mesh;
	static uint8_t count;
	uint8_t id;

public:

	static void Init();

	virtual void Draw(const DrawInfo& info) override;
};