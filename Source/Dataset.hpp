#pragma once

#include <rapidjson/rapidjson.h>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>

#include <string>
#include <vector>
#include <map>
#include <functional>

#include "Exception.hpp"

namespace json = rapidjson;
class Dataset
{
public:
    struct BlobDetails
    {
        float avgHeight;
        float minArea;
        float maxArea;
        float minDistance;
        float maxDistance;
    };

    struct FrameData
    {
        glm::vec3 position;
        glm::vec3 orientation;
        std::string rgbImage;
        std::string segImage;
        float timestamp;
    };
    std::vector<FrameData> frames;

    enum class SegmentType
    {
        None,
        Box,
        Person
    };

    struct SegmentRenderer
    {
        SegmentType type = SegmentType::None;
        union
        {
            glm::vec3 size;
            float height;
        };
    };
    struct SegmentData
    {
        SegmentRenderer renderer;
        glm::vec3 color;
        BlobDetails details;
    };


    std::map<std::string,SegmentData> segments;

    bool loaded = false;

    struct Camera
    {
        glm::ivec2 resolution;
        float fov;
        int32_t frameRate;
    } cam;

    void Load(const char* filename, std::function<bool(std::string, int, int)> progressCallback);

    void Reset();
};