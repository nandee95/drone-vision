#pragma once

#include <glad/glad.h>
#include <glm/gtc/type_ptr.hpp>

#include <array>
#include <iostream>

#include "Exception.hpp"

#include <cppfs/fs.h>
#include <cppfs/FileHandle.h>

class Shader
{
protected:
	GLuint program = 0;

public:
	Shader()
	{}

	Shader(Shader& other) = delete;
	Shader(Shader&& other) noexcept : program(other.program)
	{
		other.program = 0;
	}

	Shader(const char* vertFile, const char* fragFile)
	{
		Create(vertFile, fragFile);
	}

	void Create(const char* vertFile,const char* fragFile)
	{
		// Read files
		auto vertBuffer = ReadFile(vertFile);
		auto fragBuffer = ReadFile(fragFile);

		// Create program
		if ((program = glCreateProgram()) == 0)
		{
			throw Exception(E::FailedToCreateShader, "Failed to create shader program");
		}
		glUseProgram(program);

		// Create vertex shader
		const GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
		if (vertShader == 0)
		{
			throw Exception(E::FailedToCreateShader, "Failed to create vertex shader");
		}
		glAttachShader(program, vertShader);

		glShaderSource(vertShader, 1, &std::array<const char*, 1> {  vertBuffer.c_str() } [0] , NULL);
		glCompileShader(vertShader);

		GLint success = 0;
		glGetShaderiv(vertShader, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			char infoLog[512];
			glGetShaderInfoLog(vertShader, 512, NULL, infoLog);
			std::cout << infoLog << std::endl;

			glDeleteShader(vertShader);
			throw Exception(E::FailedToCreateShader, "Failed to compile vertex shader");
		}
		glDeleteShader(vertShader);

		// Create fragment shader
		const GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
		if (fragShader == 0)
		{
			throw Exception(E::FailedToCreateShader, "Failed to create fragment shader");
		}
		glAttachShader(program, fragShader);

		glShaderSource(fragShader, 1, &std::array<const char*, 1> {  fragBuffer.c_str() } [0] , NULL);
		glCompileShader(fragShader);

		glGetShaderiv(fragShader, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			std::cerr << "Failed to compile fragment shader" << std::endl;
			char infoLog[512];
			glGetShaderInfoLog(fragShader, 512, NULL, infoLog);

			std::cerr << infoLog << std::endl;
			glDeleteShader(fragShader);
			throw Exception(E::FailedToCreateShader, "Failed to compile fragment shader");
		}
		glDeleteShader(fragShader);

		// Link shaders
		glLinkProgram(program);

		glGetProgramiv(program, GL_LINK_STATUS, &success);
		if (!success)
		{
			char infoLog[512];
			glGetProgramInfoLog(program, 512, NULL, infoLog);
			std::cout << infoLog << std::endl;
			throw Exception(E::FailedToCreateShader, "Failed to link shaders");
		}
	}

	void Destroy()
	{
		if (program != 0)
		{
			glDeleteShader(program);
		}
	}

	~Shader()
	{
		Destroy();
	}

	void Use() const
	{
		glUseProgram(program);
	}

	void SetUniform(const int32_t& location, const glm::mat4& value)
	{
		glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(value));
	}

	void SetUniform(const int32_t& location, const glm::vec4& value)
	{
		glUniform4fv(location, 1, glm::value_ptr(value));
	}

	void SetUniform(const int32_t& location, const int32_t& value)
	{
		glUniform1i(location,value);
	}

	bool IsValid() const
	{
		return program != 0;
	}

	operator bool() const
	{
		return program != 0;
	}

	Shader& operator=(Shader&& other) noexcept
	{
		Destroy();
		program = other.program;
		other.program = 0;
		return *this;
	}

protected:
	static std::string ReadFile(const char* filename)
	{
		auto file = cppfs::fs::open(filename);

		if (!file.isFile())
		{
			throw Exception(E::FailedToOpenFile,"Failed to open file \"",filename,'"');
		}

		if (file.size() > 5e6) // 5Mb
		{
			throw Exception(E::FileIsTooLarge, "Shader file \"", filename, "\" is too large (>5Mb)");
		}

		return file.readFile();
	}
};