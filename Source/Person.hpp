#pragma once
#include "Object.hpp"
#include "VertexArray.hpp"
#include "ObjMesh.hpp"

#include <glm/vec3.hpp>
#include <glm/gtc/constants.hpp>

#include <memory>
#include <vector>

class Person : public Object
{
    static constexpr const float height = 1700.f;
    static constexpr const float textDistance = 400;

    static ObjMesh mesh;

    static int32_t count;

    struct Vertex
    {
        glm::vec3 position;
        glm::vec3 normal;
    };
public:
    static void Init();

    virtual void Draw(const DrawInfo& info) override;
};

