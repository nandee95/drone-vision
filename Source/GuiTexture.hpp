#pragma once

#include <glad/glad.h>
#include <glm/vec2.hpp>
#include <turbojpeg.h>
#include <spng.h>

#include <string>
#include <vector>
#include <mutex>

class GuiTexture
{
    static tjhandle jpegHandle;
    bool changed = false;

    std::mutex bufferMutex;
public:
    std::vector<char> rgbBuffer;

    glm::ivec2 resolution;
	GLuint texture;
    void Create();

    void Clear();
    void Update();
    void SetImage(const std::string& filename);
    static void FlipImage(char* data, const uint32_t& lineLen, const uint32_t& lines);
};