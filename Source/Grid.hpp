#pragma once
#include <stdint.h>

#include "Shader.hpp"
#include "Drawable.hpp"

class Grid : public Drawable
{
    GLuint VAO;
    GLuint VBO;
    uint32_t count;
    Shader shader;
public:
    Grid();
    virtual void Draw(const DrawInfo& info) override;
};