#pragma once

#include <stdint.h>

#include <glad/glad.h>
#include <nanogui/nanogui.h>

#define GLM_ENABLE_EXPERIMENTAL
#define GLM_FORCE_RADIANS 
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/intersect.hpp>

#include <iostream>
#include <algorithm>
#include <chrono>
#include <memory>
#include <map>
#include <mutex>
#include <thread>


#include "Camera.hpp"
#include "Grid.hpp"
#include "Shader.hpp"
#include "UniformBuffer.hpp"
#include "Person.hpp"
#include "Box.hpp"
#include "Drone.hpp"
#include "Dataset.hpp"

#include "ParserWindow.hpp"
#include "PlaybackWindow.hpp"
#include "SegmentWindow.hpp"

#include "CoordinateSystem.hpp"
#include "BlobDetector.hpp"

class Application
{
public:
	static constexpr float const cameraSensitivity = 0.02;
	static constexpr float const cameraSpeed = 5000;

	struct CameraUniformData
	{
		glm::mat4 projView;
		glm::vec3 cameraPos;
	} cameraData;

	glm::mat4 projection;

	//Graphics
	GLFWwindow* window;
	UniformBuffer cameraUniform;
	Camera camera;
	glm::vec2 oldCursorPos{ 0,0 };

	// Objects
	Drone drone;

	// GUI
	std::mutex guiMutex;
	nanogui::Screen* screen = nullptr;
	bool mouseLocked = false;
	GLFWcursor* moveCursor, * arrowCursor;
	glm::ivec2 oldWindowPos, oldWindowSize;
	std::vector<GuiWindow*> windows;

	// JSON parsing
	ParserWindow parserWindow;
	PlaybackWindow playbackWindow;
	SegmentWindow segmentWindow;
	std::thread parserThread;
	bool parsingInProgress = false;
	Dataset dataset;

	// Playback
	uint32_t currentFrame = 0;
	bool playing = false;
	std::thread playbackThread;
	bool cameraLocked = false;

	// Scene
	BlobDetector detector;
	struct ObjectData
	{
		Dataset::BlobDetails blobData;
		std::vector<std::shared_ptr<Object>> objects;
	};
	std::map<std::string, ObjectData> objects;
	std::mutex objectMutex;

	Application();
	~Application();

	int32_t Run();
	void SetGuiState(const GuiState& state);

	static void PlaybackThread(Application* that, bool singleFrame);
	static void ParserThread(Application* that, const std::string& path);

	void RegisterGuiCallbacks();
	void RegisterWindowCallbacks();

	glm::vec3 CalcWorldVector(const glm::vec2& screenPos, const glm::vec2& screenResolution,
		const glm::quat& orientation) const;
};