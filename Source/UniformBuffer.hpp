#pragma once

#include <glad/glad.h>

class UniformBuffer
{
protected:
    GLuint UBO = 0;
public:
    UniformBuffer()
    {
        
    }

    UniformBuffer(UniformBuffer& other) = delete;

    UniformBuffer(UniformBuffer&& other) noexcept : UBO(other.UBO) {
        other.UBO = 0;
    }

    void Create(const void* data,const size_t& size, const GLenum& updateRate = GL_STREAM_DRAW) noexcept
    {
        glCreateBuffers(1,&UBO);
        glBindBuffer(GL_UNIFORM_BUFFER,UBO);
        glBufferData(GL_UNIFORM_BUFFER, size, data, updateRate);
    }

    void BindBufferBase(GLuint bindingPoint) noexcept
    {
        glBindBufferBase(GL_UNIFORM_BUFFER, bindingPoint, UBO); 
    }

    void Upload(const void* data,const size_t& offset, const size_t& size) noexcept
    {
        glBindBuffer(GL_UNIFORM_BUFFER, UBO);
        glBufferSubData(GL_UNIFORM_BUFFER, offset, size, data); 
    }

    bool IsValid() const
    {
        return UBO != 0;
    }

    operator bool()const
    {
        return UBO != 0;
    }

    UniformBuffer& operator = (UniformBuffer&& other) noexcept
    {
        Destroy();
        UBO = other.UBO;
        other.UBO = 0;
        return *this;
    }

    ~UniformBuffer()
    {
        Destroy();
    }

    void Destroy() noexcept
    {
        if(UBO != 0)
        {
            glDeleteBuffers(1,&UBO);
            UBO = 0;
        }
    }

};
