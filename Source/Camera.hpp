#pragma once

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/string_cast.hpp>

class Camera
{
    float aspectRatio = 1.f;
    float fov = glm::radians(90.f);
    glm::vec3 position={0,0,0};
    glm::quat orientation={1,0,0,0};
public:
	const void SetPosition(const glm::vec3& position)
	{
		this->position = position;
	}

	const glm::vec3& GetPosition() const
	{
		return position;
	}

	void SetOrientation(const glm::quat orientation)
	{
		this->orientation = orientation;
	}

	const glm::quat& GetOrientation() const
	{
		return orientation;
	}

    void SetFOV(const float& fov)
    {
        this->fov = fov;
    }

    const float& GetFOV() const
    {
        return fov;
    }

    void SetAspectRatio(const float& aspectRatio)
    {
        this->aspectRatio = aspectRatio;
    }

    const float& GetSetAspectRatio() const
    {
        return aspectRatio;
    }
	
	const void Turn(glm::vec3 angle)
	{
		orientation = glm::rotate(glm::normalize(orientation), angle.x, glm::vec3(1, 0, 0) * orientation);
		orientation = glm::rotate(glm::normalize(orientation), angle.y, glm::vec3(0, 1, 0));
		orientation = glm::rotate(glm::normalize(orientation), angle.z, glm::vec3(0, 0, 1) * orientation);
	}

	const void Turn2(glm::vec3 angle)
	{
		orientation = glm::rotate(glm::normalize(orientation), angle.x, glm::vec3(1, 0, 0));
		orientation = glm::rotate(glm::normalize(orientation), angle.y, glm::vec3(0, 1, 0));
		orientation = glm::rotate(glm::normalize(orientation), angle.z, glm::vec3(0, 0, 1));
	}

	const void Move(glm::vec3 direction)
	{
		position += (glm::vec3(1, 0, 0) * orientation) * direction.x;
		position += (glm::vec3(0, 1, 0) * orientation) * direction.y;
		position += (glm::vec3(0, 0, 1) * orientation) * direction.z;
	}

	const glm::highp_mat4 CalcViewMatrix() const
	{
		const glm::vec3 eye = position;
		return glm::lookAt(eye, eye + (glm::vec3(0, 0, 1) * orientation), (glm::vec3(0, -1, 0) * orientation));
	}

	const glm::mat4 CalcProjMatrix(const float& nearPlane=0.1f,const float& farPlane = 1e6) const
	{
		auto proj =  glm::perspective(fov, aspectRatio, nearPlane, farPlane);
		proj[1][1] *= -1;
		return proj;
	}
};
