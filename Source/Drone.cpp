#define GLM_FORCE_RADIANS
#include "Drone.hpp"

#include <math.h>
#include <glm/gtx/string_cast.hpp>

void Drone::Create()
{
	mesh.Create("../../Resources/Drone.obj");
	frustum.Create("../../Resources/Frustum.obj");
	frustum.SetColor({ 1,1,0.3,0.4 });
	frustum.SetScale({ 1000,1000,1000 });

	rayShader.Create("../../Shaders/Ray.vert", "../../Shaders/Ray.frag");
	rayArray.Create();
}

void Drone::SetFov(const float& hfov, const float& vfov)
{
	frustum.SetScale(glm::vec3{ 2000 * tanf(hfov / 2),2000 * tanf(vfov / 2),1000 });
}

void Drone::Draw(const DrawInfo& info)
{
	mesh.SetColor(GetColor());

	const auto unit = glm::vec3({ 1,0,0 }) * GetOrientation();

	const auto yaw = std::atan2f(unit.z,unit.x);

	mesh.SetOrientation({0,yaw ,0});
	mesh.SetPosition(GetPosition() + glm::vec3(0,100,0));

	mesh.Draw(info);


	rayShader.Use();
	UpdateModelMatrix();
	rayShader.SetUniform(0, model);
	rayArray.Bind();
	rayMutex.lock();
	rayArray.FillVertices(rays);
	rayArray.SetAttribPointers({ {3,GL_FLOAT},{3,GL_FLOAT} });
	rayArray.DrawArrays(GL_LINES, 0, rays.size() * 2);
	rayMutex.unlock();

	frustum.SetOrientation(GetOrientation());
	frustum.SetPosition(GetPosition());


	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	frustum.SetColor({ 1,1,0.3,0.25 });
	frustum.Draw(info);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	frustum.SetColor({ 1,1,0.3,0.15 });
	frustum.Draw(info);

}
