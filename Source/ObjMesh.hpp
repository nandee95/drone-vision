#pragma once

#include <stdio.h>
#include <stdlib.h>

#include <vector>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/gtx/normal.hpp>

#include "Exception.hpp"
#include "VertexArray.hpp"
#include "Shader.hpp"
#include "Drawable.hpp"
#include "Colorable.hpp"
#include "Transformable.hpp"

class ObjMesh : public Drawable, public Transformable, public Colorable
{
	static Shader shader;
	VertexArray va;
	int32_t count;
public:
	static void Init();

	void Create(const char* filename);

	virtual void Draw(const DrawInfo& info) override;
};
