#include "Application.hpp"
#include "Exception.hpp"

#include <stdint.h>

int32_t main()
{
    int32_t result = EXIT_SUCCESS;
    try
    {
        Application app;
        result = app.Run();
    }
    catch (Exception& e)
    {
        std::cerr << "Unhandled exception: " << e.GetMessage() << std::endl;
    }
    return result;
}