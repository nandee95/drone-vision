#pragma once

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/quaternion.hpp>

class Transformable
{
private:
    glm::mat4 orientationMatrix { 1.f };
    bool requiresUpdate = true;
    bool hasOrigin = false;
protected:
    glm::vec3 position{0.f,0.f,0.f};
    glm::vec3 origin{0.f,0.f,0.f};
    glm::vec3 scale{1.f,1.f,1.f};
    glm::quat orientation{1.f,0.f,0.f,0.f};
public:
    glm::mat4 model{ 1.f };
    const glm::vec3& GetPosition() const 
    {
        return position;
    }

    const glm::quat& GetOrientation() const 
    {
        return orientation;
    }

    const glm::vec3& GetScale() const 
    {
        return scale;
    }

    const glm::vec3 GetOrigin() const
    {
        return origin;
    }

    const void SetOrientation(const glm::vec3& orientation)
    {
        this->orientation = orientation;
        requiresUpdate = true;
    }

    const void SetOrientation(const glm::quat& orientation)
    {
        this->orientation = orientation;
        requiresUpdate = true;
    }

    const void SetScale(const glm::vec3& scale)
    {
        this->scale = scale;
        requiresUpdate = true;
    }
    const void SetScale(const float& scale)
    {
        this->scale = {scale,scale,scale};
        requiresUpdate = true;
    }

    const void SetOrigin(const glm::vec3& origin)
    {
        this->origin = origin;
        requiresUpdate = true;
        hasOrigin = origin != glm::vec3(0.f,0.f,0.f);
    }

    const void SetPosition(const glm::vec3& position)
    {
        this->position = position;
        requiresUpdate = true;
    }

    void UpdateModelMatrix()
    {
        if(!requiresUpdate) return;

        model = glm::mat4(1.f);
        model *= glm::mat4_cast(orientation);

        // Most models won't have an origin
        if(hasOrigin) model = glm::translate(model, -origin);
        model = glm::translate(model, position * orientation);
		model = glm::scale(model, scale);
        requiresUpdate = false;
    }


};


