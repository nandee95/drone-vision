#pragma once

#include <sstream>

enum class ExceptionType
{
	Unknown,
	// File
	FailedToOpenFile,
	FailedToParseFile,
	CorruptedFile,
	FileIsTooLarge,
	// Shader
	FailedToCreateShader,
	//Font
	FailedToCreateFont
};

using E = ExceptionType;

class Exception
{
protected:
	ExceptionType type;
	std::string message;
public:

	template<typename... Args>
	Exception(const ExceptionType& type,Args... args) : type(type)
	{
		std::stringstream ss;
		ProcessArgs(ss,args...);
		message = ss.str();
	}

	const std::string& GetMessage() const
	{
		return message;
	}

	const ExceptionType& GetType() const
	{
		return type;
	}

protected:
	template<typename T1, typename T2, typename... Args>
	void ProcessArgs(std::stringstream& ss, const T1& first, const T2& second, Args... rest)
	{
		ss << first;
		ProcessArgs(ss,second, rest...);
	}

	template<typename T1>
	void ProcessArgs(std::stringstream& ss, const T1& first)
	{
		ss << first;
	}

	void ProcessArgs(std::stringstream& ss)
	{

	}
};